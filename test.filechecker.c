/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.filechecker.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_filechecker(void)
{
	int
	mr, or, all, num, test;
	char
	*string;

	mr = or = all = num = test = 0;
	string = "haha";
	printf("\033[1;37m======== FILECHECKER =======\033[0m\n");

	// printf("TEST>\t%%jx -> 4294967296\n");
	// mr = ft_printf("MINE>\t%%jx", 4294967296);
	// printf("\n");
	// or = printf("ORIG>\t%jx", 4294967296);
	// printf("\n");
	// all += print_result(mr, or, __LINE__);
	// num++;

	// printf("TEST>\t%%jx -> -4294967296\n");
	// mr = ft_printf("MINE>\t%%jx", -4294967296);
	// printf("\n");
	// or = printf("ORIG>\t%jx", -4294967296);
	// printf("\n");
	// all += print_result(mr, or, __LINE__);
	// num++;

	// printf("TEST>\t%%jx -> -4294967297\n");
	// mr = ft_printf("MINE>\t%%jx", -4294967297);
	// printf("\n");
	// or = printf("ORIG>\t%jx", -4294967297);
	// printf("\n");
	// all += print_result(mr, or, __LINE__);
	// num++;

	printf("TEST>\t%.2c -> NULL\n");
	mr = ft_printf("MINE>\t%.2c", NULL);
	printf("\n");
	or = printf("%ORIG>\t%.2c", NULL);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t@moulitest: %c -> 0\n");
	mr = ft_printf("MINE>\t@moulitest: %c", 0);
	printf("\n");
	or = printf("ORIG>\t@moulitest: %c", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%2c -> 0\n");
	mr = ft_printf("MINE>\t%2c", 0);
	printf("\n");
	or = printf("ORIG>\t%2c", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\tnull %c and text -> 0\n");
	mr = ft_printf("MINE>\tnull %c and text", 0);
	printf("\n");
	or = printf("ORIG>\tnull %c and text", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t% c -> 0\n");
	mr = ft_printf("MINE>\t% c", 0);
	printf("\n");
	or = printf("%ORIG>\t% c", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%0+5d -> 42\n");
	mr = ft_printf("MINE>\t%0+5d", 42);
	printf("\n");
	or = printf("ORIG>\t%0+5d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%ld -> 2147483648\n");
	mr = ft_printf("MINE>\t%ld", 2147483648);
	printf("\n");
	or = printf("ORIG>\t%ld", 2147483648);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%ld -> -2147483649\n");
	mr = ft_printf("MINE>\t%ld", -2147483649);
	printf("\n");
	or = printf("ORIG>\t%ld", -2147483649);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%lld -> 9223372036854775807\n");
	mr = ft_printf("MINE>\t%lld", 9223372036854775807);
	printf("\n");
	or = printf("ORIG>\t%lld", 9223372036854775807);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%lld -> -9223372036854775808\n");
	mr = ft_printf("MINE>\t%lld", -9223372036854775808);
	printf("\n");
	or = printf("ORIG>\t%lld", -9223372036854775808);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%jd -> 9223372036854775807\n");
	mr = ft_printf("MINE>\t%jd", 9223372036854775807);
	printf("\n");
	or = printf("ORIG>\t%jd", 9223372036854775807);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%jd -> -9223372036854775808\n");
	mr = ft_printf("MINE>\t%jd", -9223372036854775808);
	printf("\n");
	or = printf("ORIG>\t%jd", -9223372036854775808);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%zd -> 4294967295\n");
	mr = ft_printf("MINE>\t%zd", 4294967295);
	printf("\n");
	or = printf("ORIG>\t%zd", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%zd -> 4294967296\n");
	mr = ft_printf("MINE>\t%zd", 4294967296);
	printf("\n");
	or = printf("ORIG>\t%zd", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t@moulitest: %.d %.0d -> 0, 0\n");
	mr = ft_printf("MINE>\t@moulitest: %.d %.0d", 0, 0);
	printf("\n");
	or = printf("ORIG>\t@moulitest: %.d %.0d", 0, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t@moulitest: %5.d %5.0d -> 0, 0\n");
	mr = ft_printf("MINE>\t@moulitest: %5.d %5.0d", 0, 0);
	printf("\n");
	or = printf("ORIG>\t@moulitest: %5.d %5.0d", 0, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%u -> -1\n");
	mr = ft_printf("MINE>\t%u", -1);
	printf("\n");
	or = printf("ORIG>\t%u", -1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t%%u -> 4294967296\n");
	mr = ft_printf("MINE>\t%u", 4294967296);
	printf("\n");
	or = printf("ORIG>\t%u", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	// printf("TEST>\t%%U -> 4294967295\n");
	// mr = ft_printf("MINE>\t%U", 4294967295);
	// printf("\n");
	// or = printf("ORIG>\t%U", 4294967295);
	// printf("\n");
	// all += print_result(mr, or, __LINE__);
	// num++;

	// printf("TEST>\t%%hU -> 4294967296\n");
	// mr = ft_printf("MINE>\t%hU", 4294967296);
	// printf("\n");
	// or = printf("ORIG>\t%hU", 4294967296);
	// printf("\n");
	// all += print_result(mr, or, __LINE__);
	// num++;

	// printf("TEST>\t%%U -> 4294967296\n");
	// mr = ft_printf("MINE>\t%U", 4294967296);
	// printf("\n");
	// or = printf("ORIG>\t%U", 4294967296);
	// printf("\n");
	// all += print_result(mr, or, __LINE__);
	// num++;

	printf("\"\"\n");
	mr = ft_printf("");
	printf("\n");
	or = printf("");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\\\\n\n");
	mr = ft_printf("\\n");
	printf("\n");
	or = printf("\\n");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("test\n");
	mr = ft_printf("test");
	printf("\n");
	or = printf("test");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("test\\\\n\n");
	mr = ft_printf("test\\n");
	printf("\n");
	or = printf("test\\n");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("1234\n");
	mr = ft_printf("1234");
	printf("\n");
	or = printf("1234");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%%%\n");
	mr = ft_printf("%%");
	printf("\n");
	or = printf("%%");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%5%%\n");
	mr = ft_printf("%5%");
	printf("\n");
	or = printf("%5%");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%-5%%\n");
	mr = ft_printf("%-5%");
	printf("\n");
	or = printf("%-5%");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%.0%%\n");
	mr = ft_printf("%.0%");
	printf("\n");
	or = printf("%.0%");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%%% -> test\n");
	mr = ft_printf("%%", "test");
	printf("\n");
	or = printf("%%", "test");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%   %% -> test\n");
	mr = ft_printf("%   %", "test");
	printf("\n");
	or = printf("%   %", "test");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%x -> 42\n");
	mr = ft_printf("%x", 42);
	printf("\n");
	or = printf("%x", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%X -> 42\n");
	mr = ft_printf("%X", 42);
	printf("\n");
	or = printf("%X", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%x -> 0\n");
	mr = ft_printf("%x", 0);
	printf("\n");
	or = printf("%x", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%X -> 0\n");
	mr = ft_printf("%X", 0);
	printf("\n");
	or = printf("%X", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%x -> -4\n");
	mr = ft_printf("%x", -42);
	printf("\n");
	or = printf("%x", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%X -> -42\n");
	mr = ft_printf("%X", -42);
	printf("\n");
	or = printf("%X", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%x -> 4294967296\n");
	mr = ft_printf("%x", 4294967296);
	printf("\n");
	or = printf("%x", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%X -> 4294967296\n");
	mr = ft_printf("%X", 4294967296);
	printf("\n");
	or = printf("%X", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%x -> test\n");
	mr = ft_printf("%x", test);
	printf("\n");
	or = printf("%x", test);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%10x -> 42");
	mr = ft_printf("%10x", 42);
	printf("\n");
	or = printf("%10x", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("%%-10x -> 42");
	mr = ft_printf("%-10x", 42);
	printf("\n");
	or = printf("%-10x", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%lx", 4294967296);
	printf("\n");
	or = printf("%lx", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%llX", 4294967296);
	printf("\n");
	or = printf("%llX", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%hx", 4294967296);
	printf("\n");
	or = printf("%hx", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%hhX", 4294967296);
	printf("\n");
	or = printf("%hhX", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%jx", 4294967295);
	printf("\n");
	or = printf("%jx", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	// mr = ft_printf("%jx", 4294967296);
	// printf("\n");
	// or = printf("%jx", 4294967296);
	// printf("\n");
	// all += print_result(mr, or, __LINE__);
	// num++;

	// mr = ft_printf("%jx", -4294967296);
	// printf("\n");
	// or = printf("%jx", -4294967296);
	// printf("\n");
	// all += print_result(mr, or, __LINE__);
	// num++;

	// mr = ft_printf("%jx", -4294967297);
	// printf("\n");
	// or = printf("%jx", -4294967297);
	// printf("\n");
	// all += print_result(mr, or, __LINE__);
	// num++;

	mr = ft_printf("%llx", 9223372036854775807);
	printf("\n");
	or = printf("%llx", 9223372036854775807);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%llx", 9223372036854775808);
	printf("\n");
	or = printf("%llx", 9223372036854775808);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%010x", 542);
	printf("\n");
	or = printf("%010x", 542);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-15x", 542);
	printf("\n");
	or = printf("%-15x", 542);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%2x", 542);
	printf("\n");
	or = printf("%2x", 542);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%.2x", 5427);
	printf("\n");
	or = printf("%.2x", 5427);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%5.2x", 5427);
	printf("\n");
	or = printf("%5.2x", 5427);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%#x", 42);
	printf("\n");
	or = printf("%#x", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%#llx", 9223372036854775807);
	printf("\n");
	or = printf("%#llx", 9223372036854775807);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%#x", 0);
	printf("\n");
	or = printf("%#x", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%#x", 42);
	printf("\n");
	or = printf("%#x", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%#X", 42);
	printf("\n");
	or = printf("%#X", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%#8x", 42);
	printf("\n");
	or = printf("%#8x", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%#08x", 42);
	printf("\n");
	or = printf("%#08x", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%#-08x", 42);
	printf("\n");
	or = printf("%#-08x", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %#.x %#.0x", 0, 0);
	printf("\n");
	or = printf("@moulitest: %#.x %#.0x", 0, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %.x %.0x", 0, 0);
	printf("\n");
	or = printf("@moulitest: %.x %.0x", 0, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %5.x %5.0x", 0, 0);
	printf("\n");
	or = printf("@moulitest: %5.x %5.0x", 0, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s", "abc");
	printf("\n");
	or = printf("%s", "abc");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s", "this is a string");
	printf("\n");
	or = printf("%s", "this is a string");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s ", "this is a string");
	printf("\n");
	or = printf("%s ", "this is a string");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s  ", "this is a string");
	printf("\n");
	or = printf("%s  ", "this is a string");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("this is a %s", "string");
	printf("\n");
	or = printf("this is a %s", "string");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s is a string", "this");
	printf("\n");
	or = printf("%s is a string", "this");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("Line Feed %s", "\\n");
	printf("\n");
	or = printf("Line Feed %s", "\\n");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%10s is a string", "this");
	printf("\n");
	or = printf("%10s is a string", "this");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%.2s is a string", "this");
	printf("\n");
	or = printf("%.2s is a string", "this");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%5.2s is a string", "this");
	printf("\n");
	or = printf("%5.2s is a string", "this");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%10s is a string", "");
	printf("\n");
	or = printf("%10s is a string", "");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%.2s is a string", "");
	printf("\n");
	or = printf("%.2s is a string", "");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%5.2s is a string", "");
	printf("\n");
	or = printf("%5.2s is a string", "");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-10s is a string", "this");
	printf("\n");
	or = printf("%-10s is a string", "this");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-.2s is a string", "this");
	printf("\n");
	or = printf("%-.2s is a string", "this");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-5.2s is a string", "this");
	printf("\n");
	or = printf("%-5.2s is a string", "this");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-10s is a string", "");
	printf("\n");
	or = printf("%-10s is a string", "");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-.2s is a string", "");
	printf("\n");
	or = printf("%-.2s is a string", "");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-5.2s is a string", "");
	printf("\n");
	or = printf("%-5.2s is a string", "");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s %s", "this", "is");
	printf("\n");
	or = printf("%s %s", "this", "is");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s %s %s", "this", "is", "a");
	printf("\n");
	or = printf("%s %s %s", "this", "is", "a");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s %s %s %s", "this", "is", "a", "multi");
	printf("\n");
	or = printf("%s %s %s %s", "this", "is", "a", "multi");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s %s %s %s string. gg!", "this", "is", "a", "multi", "string");
	printf("\n");
	or = printf("%s %s %s %s string. gg!", "this", "is", "a", "multi", "string");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s%s%s%s%s", "this", "is", "a", "multi", "string");
	printf("\n");
	or = printf("%s%s%s%s%s", "this", "is", "a", "multi", "string");
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %s", NULL);
	printf("\n");
	or = printf("@moulitest: %s", NULL);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%.2c", NULL);
	printf("\n");
	or = printf("%.2c", NULL);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%s %s", NULL, string);
	printf("\n");
	or = printf("%s %s", NULL, string);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%c", 42);
	printf("\n");
	or = printf("%c", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%5c", 42);
	printf("\n");
	or = printf("%5c", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-5c", 42);
	printf("\n");
	or = printf("%-5c", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %c", 0);
	printf("\n");
	or = printf("@moulitest: %c", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%2c", 0);
	printf("\n");
	or = printf("%2c", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("null %c and text", 0);
	printf("\n");
	or = printf("null %c and text", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("% c", 0);
	printf("\n");
	or = printf("% c", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%o", 40);
	printf("\n");
	or = printf("%o", 40);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%5o", 41);
	printf("\n");
	or = printf("%5o", 41);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%05o", 42);
	printf("\n");
	or = printf("%05o", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-5o", 2500);
	printf("\n");
	or = printf("%-5o", 2500);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%#6o", 2500);
	printf("\n");
	or = printf("%#6o", 2500);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-#6o", 2500);
	printf("\n");
	or = printf("%-#6o", 2500);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-05o", 2500);
	printf("\n");
	or = printf("%-05o", 2500);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-5.10o", 2500);
	printf("\n");
	or = printf("%-5.10o", 2500);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-10.5o", 2500);
	printf("\n");
	or = printf("%-10.5o", 2500);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %o", 0);
	printf("\n");
	or = printf("@moulitest: %o", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %.o %.0o", 0, 0);
	printf("\n");
	or = printf("@moulitest: %.o %.0o", 0, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %5.o %5.0o", 0, 0);
	printf("\n");
	or = printf("@moulitest: %5.o %5.0o", 0, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %#.o %#.0o", 0, 0);
	printf("\n");
	or = printf("@moulitest: %#.o %#.0o", 0, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %.10o", 42);
	printf("\n");
	or = printf("@moulitest: %.10o", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d", 1);
	printf("\n");
	or = printf("%d", 1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("the %d", 1);
	printf("\n");
	or = printf("the %d", 1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d is one", 1);
	printf("\n");
	or = printf("%d is one", 1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d", -1);
	printf("\n");
	or = printf("%d", -1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d", 4242);
	printf("\n");
	or = printf("%d", 4242);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d", -4242);
	printf("\n");
	or = printf("%d", -4242);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d", 2147483647);
	printf("\n");
	or = printf("%d", 2147483647);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d", 2147483648);
	printf("\n");
	or = printf("%d", 2147483648);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d", -2147483648);
	printf("\n");
	or = printf("%d", -2147483648);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d", -2147483649);
	printf("\n");
	or = printf("%d", -2147483649);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("% d", 42);
	printf("\n");
	or = printf("% d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("% d", -42);
	printf("\n");
	or = printf("% d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%+d", 42);
	printf("\n");
	or = printf("%+d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%+d", -42);
	printf("\n");
	or = printf("%+d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%+d", 0);
	printf("\n");
	or = printf("%+d", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("% +d", 42);
	printf("\n");
	or = printf("% +d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("% +d", -42);
	printf("\n");
	or = printf("% +d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%+ d", 42);
	printf("\n");
	or = printf("%+ d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%+ d", -42);
	printf("\n");
	or = printf("%+ d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%  +d", 42);
	printf("\n");
	or = printf("%  +d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%  +d", -42);
	printf("\n");
	or = printf("%  +d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%+  d", 42);
	printf("\n");
	or = printf("%+  d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%+  d", -42);
	printf("\n");
	or = printf("%+  d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("% ++d", 42);
	printf("\n");
	or = printf("% ++d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("% ++d", -42);
	printf("\n");
	or = printf("% ++d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%++ d", 42);
	printf("\n");
	or = printf("%++ d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%++ d", -42);
	printf("\n");
	or = printf("%++ d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%0d", -42);
	printf("\n");
	or = printf("%0d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%00d", -42);
	printf("\n");
	or = printf("%00d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%5d", 42);
	printf("\n");
	or = printf("%5d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%05d", 42);
	printf("\n");
	or = printf("%05d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%0+5d", 42);
	printf("\n");
	or = printf("%0+5d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%5d", -42);
	printf("\n");
	or = printf("%5d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%05d", -42);
	printf("\n");
	or = printf("%05d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%0+5d", -42);
	printf("\n");
	or = printf("%0+5d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-5d", 42);
	printf("\n");
	or = printf("%-5d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-05d", 42);
	printf("\n");
	or = printf("%-05d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-5d", -42);
	printf("\n");
	or = printf("%-5d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-05d", -42);
	printf("\n");
	or = printf("%-05d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%hd", 32767);
	printf("\n");
	or = printf("%hd", 32767);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%hd", 32768);
	printf("\n");
	or = printf("%hd", 32768);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%hhd", 127);
	printf("\n");
	or = printf("%hhd", 127);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%hhd", 128);
	printf("\n");
	or = printf("%hhd", 128);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%hhd", -128);
	printf("\n");
	or = printf("%hhd", -128);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%hhd", -129);
	printf("\n");
	or = printf("%hhd", -129);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%ld", 2147483647);
	printf("\n");
	or = printf("%ld", 2147483647);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%ld", -2147483648);
	printf("\n");
	or = printf("%ld", -2147483648);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%ld", 2147483648);
	printf("\n");
	or = printf("%ld", 2147483648);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%ld", -2147483649);
	printf("\n");
	or = printf("%ld", -2147483649);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%lld", 9223372036854775807);
	printf("\n");
	or = printf("%lld", 9223372036854775807);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%lld", -9223372036854775808);
	printf("\n");
	or = printf("%lld", -9223372036854775808);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%jd", 9223372036854775807);
	printf("\n");
	or = printf("%jd", 9223372036854775807);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%jd", -9223372036854775808);
	printf("\n");
	or = printf("%jd", -9223372036854775808);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%zd", 4294967295);
	printf("\n");
	or = printf("%zd", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%zd", 4294967296);
	printf("\n");
	or = printf("%zd", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%zd", -0);
	printf("\n");
	or = printf("%zd", -0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%zd", -1);
	printf("\n");
	or = printf("%zd", -1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d", 1);
	printf("\n");
	or = printf("%d", 1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d %d", 1, -2);
	printf("\n");
	or = printf("%d %d", 1, -2);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d %d %d", 1, -2, 33);
	printf("\n");
	or = printf("%d %d %d", 1, -2, 33);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d %d %d %d", 1, -2, 33, 42);
	printf("\n");
	or = printf("%d %d %d %d", 1, -2, 33, 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%d %d %d %d gg!", 1, -2, 33, 42, 0);
	printf("\n");
	or = printf("%d %d %d %d gg!", 1, -2, 33, 42, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%4.15d", 42);
	printf("\n");
	or = printf("%4.15d", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%.2d", 4242);
	printf("\n");
	or = printf("%.2d", 4242);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%.10d", 4242);
	printf("\n");
	or = printf("%.10d", 4242);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%10.5d", 4242);
	printf("\n");
	or = printf("%10.5d", 4242);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-10.5d", 4242);
	printf("\n");
	or = printf("%-10.5d", 4242);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("% 10.5d", 4242);
	printf("\n");
	or = printf("% 10.5d", 4242);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%+10.5d", 4242);
	printf("\n");
	or = printf("%+10.5d", 4242);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-+10.5d", 4242);
	printf("\n");
	or = printf("%-+10.5d", 4242);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%03.2d", 0);
	printf("\n");
	or = printf("%03.2d", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%03.2d", 1);
	printf("\n");
	or = printf("%03.2d", 1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%03.2d", -1);
	printf("\n");
	or = printf("%03.2d", -1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %.10d", -42);
	printf("\n");
	or = printf("@moulitest: %.10d", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %.d %.0d", 42, 43);
	printf("\n");
	or = printf("@moulitest: %.d %.0d", 42, 43);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %.d %.0d", 0, 0);
	printf("\n");
	or = printf("@moulitest: %.d %.0d", 0, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %5.d %5.0d", 0, 0);
	printf("\n");
	or = printf("@moulitest: %5.d %5.0d", 0, 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%u", 0);
	printf("\n");
	or = printf("%u", 0);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%u", 1);
	printf("\n");
	or = printf("%u", 1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%u", -1);
	printf("\n");
	or = printf("%u", -1);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%u", 4294967295);
	printf("\n");
	or = printf("%u", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%u", 4294967296);
	printf("\n");
	or = printf("%u", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%5u", 4294967295);
	printf("\n");
	or = printf("%5u", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%15u", 4294967295);
	printf("\n");
	or = printf("%15u", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%-15u", 4294967295);
	printf("\n");
	or = printf("%-15u", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%015u", 4294967295);
	printf("\n");
	or = printf("%015u", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("% u", 4294967295);
	printf("\n");
	or = printf("% u", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%+u", 4294967295);
	printf("\n");
	or = printf("%+u", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%lu", 4294967295);
	printf("\n");
	or = printf("%lu", 4294967295);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%lu", 4294967296);
	printf("\n");
	or = printf("%lu", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%lu", -42);
	printf("\n");
	or = printf("%lu", -42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%llu", 4999999999);
	printf("\n");
	or = printf("%llu", 4999999999);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%ju", 4999999999);
	printf("\n");
	or = printf("%ju", 4999999999);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("%ju", 4294967296);
	printf("\n");
	or = printf("%ju", 4294967296);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("@moulitest: %.5u", 42);
	printf("\n");
	or = printf("@moulitest: %.5u", 42);
	printf("\n");
	all += print_result(mr, or, __LINE__);
	num++;

	if (num == all)
		printf("\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end FILECHECKER =======\033[0m\n");
	return (all);
}
