/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.octal.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 14:43:17 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/01 12:29:56 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_octal(void)
{
	int
	mr, or, all, num;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;
	printf("\033[1;37m======== %%o ========\033[0m\n");

	printf("TEST>\t[%%o] -> 2048\n");
	mr = ft_printf("MINE>\t[%o]\n", 2048);
	or = printf("ORIG>\t[%o]\n", 2048);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%% o] -> 777\n");
	mr = ft_printf("MINE>\t[% o]\n", 777);
	or = printf("ORIG>\t[% o]\n", 777);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%+o] -> 128\n");
	mr = ft_printf("MINE>\t[%+o]\n", 128);
	or = printf("ORIG>\t[%+o]\n", 128);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%-o] -> 48\n");
	mr = ft_printf("MINE>\t[%-o]\n", 48);
	or = printf("ORIG>\t[%-o]\n", 48);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%+5o] -> 10\n");
	mr = ft_printf("MINE>\t[%+5o]\n", 10);
	or = printf("ORIG>\t[%+5o]\n", 10);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%-5o] -> 2048\n");
	mr = ft_printf("MINE>\t[%-5o]\n", 2048);
	or = printf("ORIG>\t[%-5o]\n", 2048);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%+10o] -> 2048\n");
	mr = ft_printf("MINE>\t[%+10o]\n", 2048);
	or = printf("ORIG>\t[%+10o]\n", 2048);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%-+10o] -> 2048\n");
	mr = ft_printf("MINE>\t[%-+10o]\n", 2048);
	or = printf("ORIG>\t[%-+10o]\n", 2048);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%-0+10x] -> 2048\n");
	mr = ft_printf("MINE>\t[%-0+10x]\n", 2048);
	or = printf("ORIG>\t[%-0+10x]\n", 2048);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%-0+10o] -> 2048\n");
	mr = ft_printf("MINE>\t[%-0+10o]\n", 2048);
	or = printf("ORIG>\t[%-0+10o]\n", 2048);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%#o] -> 48\n");
	mr = ft_printf("MINE>\t[%#o]\n", 48);
	or = printf("ORIG>\t[%#o]\n", 48);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%#+10o] -> 2048\n");
	mr = ft_printf("MINE>\t[%#+10o]\n", 2048);
	or = printf("ORIG>\t[%#+10o]\n", 2048);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%#-0+10o] -> 2048\n");
	mr = ft_printf("MINE>\t[%#-0+10o]\n", 2048);
	or = printf("ORIG>\t[%#-0+10o]\n", 2048);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%-0#+3o] -> 2048\n");
	mr = ft_printf("MINE>\t[%-0#+3o]\n", 2048);
	or = printf("ORIG>\t[%-0#+3o]\n", 2048);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%o] -> 02047\n");
	mr = ft_printf("MINE>\t[%o]\n", 02047);
	or = printf("ORIG>\t[%o]\n", 02047);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%o] -> 02047\n");
	mr = ft_printf("MINE>\t[%o]\n", 02047);
	or = printf("ORIG>\t[%o]\n", 02047);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%20.10o] -> 777\n");
	mr = ft_printf("MINE>\t[%20.10o]\n", 777);
	or = printf("ORIG>\t[%20.10o]\n", 777);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%+20.10o] -> 777\n");
	mr = ft_printf("MINE>\t[%+20.10o]\n", 777);
	or = printf("ORIG>\t[%+20.10o]\n", 777);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%20.10o] -> -777\n");
	mr = ft_printf("MINE>\t[%20.10o]\n", -777);
	or = printf("ORIG>\t[%20.10o]\n", -777);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%+20.10o] -> -777\n");
	mr = ft_printf("MINE>\t[%+20.10o]\n", -777);
	or = printf("ORIG>\t[%+20.10o]\n", -777);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2o] -> -127\n");
	mr = ft_printf("[%03.2o]\n", -127);
	or = printf("[%03.2o]\n", -127);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2lo] -> 0\n");
	mr = ft_printf("[%03.2o]\n", 0);
	or = printf("[%03.2o]\n", 0);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2lx] -> 0\n");
	mr = ft_printf("[%03.2x]\n", 0);
	or = printf("[%03.2x]\n", 0);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2lo] -> 127\n");
	mr = ft_printf("[%03.2o]\n", 127);
	or = printf("[%03.2o]\n", 127);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2lo] -> -127\n");
	mr = ft_printf("[%03.2o]\n", -127);
	or = printf("[%03.2o]\n", -127);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.20lo] -> 127\n");
	mr = ft_printf("[%03.2o]\n", 127);
	or = printf("[%03.2o]\n", 127);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.20lo] -> -127\n");
	mr = ft_printf("[%03.2o]\n", -127);
	or = printf("[%03.2o]\n", -127);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2llo] -> 0\n");
	mr = ft_printf("[%03.2o]\n", 0);
	or = printf("[%03.2o]\n", 0);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2llo] -> 127\n");
	mr = ft_printf("[%03.2o]\n", 127);
	or = printf("[%03.2o]\n", 127);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2llo] -> -127\n");
	mr = ft_printf("[%03.2o]\n", -127);
	or = printf("[%03.2o]\n", -127);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.20llo] -> 127\n");
	mr = ft_printf("[%03.2o]\n", 127);
	or = printf("[%03.2o]\n", 127);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.20llo] -> -127\n");
	mr = ft_printf("[%03.2o]\n", -127);
	or = printf("[%03.2o]\n", -127);
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%010o] -> 2048\n");
	mr = ft_printf("MINE>\t[%010o]\n", 2048);
	or = printf("ORIG>\t[%010o]\n", 2048);
	all += print_result(mr, or, __LINE__);
	num++;
	num++;

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end %%o =====\033[0m\n\n");
	return (all);
}
