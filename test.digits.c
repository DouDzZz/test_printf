/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.digits.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 11:34:26 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/01 17:10:01 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_digits(void)
{
	int
	mr, or, all, num;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;
	printf("\033[1;37m======== %%d ========\033[0m\n");

	printf("\nTEST: %%d -> 650\n");
	mr = ft_printf("MINE>\t[%d]\n", 650);
	or = printf("ORIG>\t[%d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%d -> -650\n");
	mr = ft_printf("MINE>\t[%d]\n", -650);
	or = printf("ORIG>\t[%d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+d -> 650\n");
	mr = ft_printf("MINE>\t[%+d]\n", 650);
	or = printf("ORIG>\t[%+d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+1d -> 650\n");
	mr = ft_printf("MINE>\t[%+1d]\n", 650);
	or = printf("ORIG>\t[%+1d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+10d -> 650\n");
	mr = ft_printf("MINE>\t[%+10d]\n", 650);
	or = printf("ORIG>\t[%+10d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+10d -> -650\n");
	mr = ft_printf("MINE>\t[%+10d]\n", -650);
	or = printf("ORIG>\t[%+10d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%10d -> 650\n");
	mr = ft_printf("MINE>\t[%10d]\n", 650);
	or = printf("ORIG>\t[%10d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-10d -> 650\n");
	mr = ft_printf("MINE>\t[%-10d]\n", 650);
	or = printf("ORIG>\t[%-10d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-5d -> 650\n");
	mr = ft_printf("MINE>\t[%-5d]\n", 650);
	or = printf("ORIG>\t[%-5d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% d -> 650\n");
	mr = ft_printf("MINE>\t[% d]\n", 650);
	or = printf("ORIG>\t[% d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% 5d -> 650\n");
	mr = ft_printf("MINE>\t[% 5d]\n", 650);
	or = printf("ORIG>\t[% 5d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% d -> -650\n");
	mr = ft_printf("MINE>\t[% d]\n", -650);
	or = printf("ORIG>\t[% d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% 5dd -> -650\n");
	mr = ft_printf("MINE>\t[% 5d]\n", -650);
	or = printf("ORIG>\t[% 5d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%05d -> -650\n");
	mr = ft_printf("MINE>\t[%05d]\n", -650);
	or = printf("ORIG>\t[%05d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+05d -> -650\n");
	mr = ft_printf("MINE>\t[%+05d]\n", -650);
	or = printf("ORIG>\t[%+05d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-05d -> -650\n");
	mr = ft_printf("MINE>\t[%-05d]\n", -650);
	or = printf("ORIG>\t[%-05d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-05d -> -650\n");
	mr = ft_printf("MINE>\t[%+-05d]\n", -650);
	or = printf("ORIG>\t[%+-05d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-09d -> -650\n");
	mr = ft_printf("MINE>\t[%+-09d]\n", -650);
	or = printf("ORIG>\t[%+-09d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+09d -> -650\n");
	mr = ft_printf("MINE>\t[%+09d]\n", -650);
	or = printf("ORIG>\t[%+09d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-1d -> -650\n");
	mr = ft_printf("MINE>\t[%+-1d]\n", -650);
	or = printf("ORIG>\t[%+-1d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+01d -> -650\n");
	mr = ft_printf("MINE>\t[%+01d]\n", -650);
	or = printf("ORIG>\t[%+01d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-20.10d -> -650\n");
	mr = ft_printf("MINE>\t[%+-20.10d]\n", -650);
	or = printf("ORIG>\t[%+-20.10d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+20.10d -> -650\n");
	mr = ft_printf("MINE>\t[%+20.10d]\n", -650);
	or = printf("ORIG>\t[%+20.10d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%20.10d -> -650\n");
	mr = ft_printf("MINE>\t[%20.10d]\n", -650);
	or = printf("ORIG>\t[%20.10d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% 20.10d -> -650\n");
	mr = ft_printf("MINE>\t[% 20.10d]\n", -650);
	or = printf("ORIG>\t[% 20.10d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% +20.10d -> -650\n");
	mr = ft_printf("MINE>\t[% +20.10d]\n", -650);
	or = printf("ORIG>\t[% +20.10d]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-20.10d -> 650\n");
	mr = ft_printf("MINE>\t[%+-20.10d]\n", 650);
	or = printf("ORIG>\t[%+-20.10d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+20.10d -> 650\n");
	mr = ft_printf("MINE>\t[%+20.10d]\n", 650);
	or = printf("ORIG>\t[%+20.10d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%20.10d -> 650\n");
	mr = ft_printf("MINE>\t[%20.10d]\n", 650);
	or = printf("ORIG>\t[%20.10d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% +20.10d -> 650\n");
	mr = ft_printf("MINE>\t[% +20.10d]\n", 650);
	or = printf("ORIG>\t[% +20.10d]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%'d -> 65000\n");
	mr = ft_printf("MINE>\t[%'d]\n", 65000);
	or = printf("ORIG>\t[%'d]\n", 65000);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%'d -> 6500000\n");
	mr = ft_printf("MINE>\t[%'d]\n", 6500000);
	or = printf("ORIG>\t[%'d]\n", 6500000);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+'d -> 65000\n");
	mr = ft_printf("MINE>\t[%+'d]\n", 65000);
	or = printf("ORIG>\t[%+'d]\n", 65000);
	all += print_result(mr, or, __LINE__);
	num++;


	printf("\nTEST>\t[%%+5d] -> 42\n");
	mr = ft_printf("[%+5d]\n", 42);
	or = printf("[%+5d]\n", 42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%05d] -> 42\n");
	mr = ft_printf("[%05d]\n", 42);
	or = printf("[%05d]\n", 42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%0+5d] -> 42\n");
	mr = ft_printf("[%0+5d]\n", 42);
	or = printf("[%0+5d]\n", 42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%+5d] -> -42\n");
	mr = ft_printf("[%+5d]\n", -42);
	or = printf("[%+5d]\n", -42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%05d] -> -42\n");
	mr = ft_printf("[%05d]\n", -42);
	or = printf("[%05d]\n", -42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%0+5d] -> -42\n");
	mr = ft_printf("[%0+5d]\n", -42);
	or = printf("[%0+5d]\n", -42);
	all += print_result(mr, or, __LINE__);
	num++;
	
	printf("\nTEST>\t[%%+d] -> -4242\n");
	mr = ft_printf("[%+d]\n", -4242);
	or = printf("[%+d]\n", -4242);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%5d] -> -4242\n");
	mr = ft_printf("[%5d]\n", -4242);
	or = printf("[%5d]\n", -4242);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%+10.7d] -> 4242\n");
	mr = ft_printf("[%+10.7d]\n", 4242);
	or = printf("[%+10.7d]\n", 4242);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%+10.5d] -> 4242\n");
	mr = ft_printf("[%+10.5d]\n", 4242);
	or = printf("[%+10.5d]\n", 4242);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%4.15d] -> 42\n");
	mr = ft_printf("[%4.15d]\n", 42);
	or = printf("[%4.15d]\n", 42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%10.5d] -> 4242\n");
	mr = ft_printf("[%10.5d]\n", 4242);
	or = printf("[%10.5d]\n", 4242);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%-10.5d] -> 4242\n");
	mr = ft_printf("[%-10.5d]\n", 4242);
	or = printf("[%-10.5d]\n", 4242);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2d] -> 0\n");
	mr = ft_printf("[%03.2d]\n", 0);
	or = printf("[%03.2d]\n", 0);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%+.5d] -> -4242\n");
	mr = ft_printf("[%+.5d]\n", -4242);
	or = printf("[%+.5d]\n", -4242);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%+.5d] -> 4242\n");
	mr = ft_printf("[%+.5d]\n", 4242);
	or = printf("[%+.5d]\n", 4242);
	all += print_result(mr, or, __LINE__);
	num++;
	
	printf("\nTEST>\t[%% 10.5d] -> 4242\n");
	mr = ft_printf("[% 10.5d]\n", 4242);
	or = printf("[% 10.5d]\n", 4242);
	all += print_result(mr, or, __LINE__);
	num++;
	
	printf("\nTEST>\t[%% 10.4d] -> 4242\n");
	mr = ft_printf("[% 10.4d]\n", 4242);
	or = printf("[% 10.4d]\n", 4242);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[@moulitest: [%%.10d] -> -42\n");
	mr = ft_printf("@moulitest: [%.10d]\n", -42);
	or = printf("@moulitest: [%.10d]\n", -42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[@moulitest: [%%.3d] -> -42\n");
	mr = ft_printf("@moulitest: [%.3d]\n", -42);
	or = printf("@moulitest: [%.3d]\n", -42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2d] -> -3\n");
	mr = ft_printf("MINE>\t[%03.2d]\n", -3);
	or = printf("ORIG>\t[%03.2d]\n", -3);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2d] -> -2\n");
	mr = ft_printf("MINE>\t[%03.2d]\n", -2);
	or = printf("ORIG>\t[%03.2d]\n", -2);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2d] -> -1\n");
	mr = ft_printf("MINE>\t[%03.2d]\n", -1);
	or = printf("ORIG>\t[%03.2d]\n", -1);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2d] -> 0\n");
	mr = ft_printf("MINE>\t[%03.2d]\n", 0);
	or = printf("ORIG>\t[%03.2d]\n", 0);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%03.2d] -> 1\n");
	mr = ft_printf("MINE>\t[%03.2d]\n", 1);
	or = printf("ORIG>\t[%03.2d]\n", 1);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%15.4d] -> 42\n");
	mr = ft_printf("MINE>\t[%15.4d]\n", 42);
	or = printf("ORIG>\t[%15.4d]\n", 42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%15.4d] -> -42\n");
	mr = ft_printf("MINE>\t[%15.4d]\n", -42);
	or = printf("ORIG>\t[%15.4d]\n", -42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%% 15.4d] -> 42\n");
	mr = ft_printf("MINE>\t[% 15.4d]\n", 42);
	or = printf("ORIG>\t[% 15.4d]\n", 42);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%% 15.4d] -> -42\n");
	mr = ft_printf("MINE>\t[% 15.4d]\n", -42);
	or = printf("ORIG>\t[% 15.4d]\n", -42);
	all += print_result(mr, or, __LINE__);
	num++;

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end %%d =====\033[0m\n\n");
	return (all);
}
