# 42 PRINTF TESTER

## installation
- Put the folder in printf root
- Go in tests folder
- Run run.test.sh

```bash
?> sh run.test.sh -h
Usage:  test    [[-avhrR]|test_nb]


		options:
				-a: run all tests
				-v: verbose mode
				-h: print usage
				-r: rebuild printf
				-b: rebuild all

		tests index:
				0: run all tests
				1: test digits
				2: test unsigned
				3: test strings
				4: test chars
				5: test octal
				6: test hexadecimal
				7: test hexadecimal HARD
				8: test memory
				9: test width parameters
				10: test crashtest
				11: test modifiers
				12: test precision
```

# Author
Edouard Jubert

edjubert@student.42.fr
