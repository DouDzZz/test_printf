/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.precision.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 15:35:16 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/28 18:12:14 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_precision(void)
{
	int
	mr, or, all, num;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;

	printf("\033[1;37m==== precision modifiers ===\033[0m\n");

	printf("TEST>\t[%%10.4d] -> 10\n");
	mr = ft_printf("MINE>\t[%10.4d]\n", 10);
	or = printf(   "ORIG>\t[%10.4d]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%.4d] -> 10\n");
	mr = ft_printf("MINE>\t[%.4d]\n", 10);
	or = printf(   "ORIG>\t[%.4d]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-.4d] -> 10000\n");
	mr = ft_printf("MINE>\t[%-.4d]\n", 10000);
	or = printf(   "ORIG>\t[%-.4d]\n", 10000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%.1d] -> 10000\n");
	mr = ft_printf("MINE>\t[%.1d]\n", 10000);
	or = printf(   "ORIG>\t[%.1d]\n", 10000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-.0d] -> 10000\n");
	mr = ft_printf("MINE>\t[%-.0d]\n", 10000);
	or = printf(   "ORIG>\t[%-.0d]\n", 10000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%5.4d] -> 10\n");
	mr = ft_printf("MINE>\t[%5.4d]\n", 10);
	or = printf(   "ORIG>\t[%5.4d]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-8.4d] -> 10\n");
	mr = ft_printf("MINE>\t[%-8.4d]\n", 10);
	or = printf(   "ORIG>\t[%-8.4d]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-5.4d] -> 10\n");
	mr = ft_printf("MINE>\t[%-5.4d]\n", 10);
	or = printf(   "ORIG>\t[%-5.4d]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-+8.4d] -> 10\n");
	mr = ft_printf("MINE>\t[%-+8.4d]\n", 10);
	or = printf(   "ORIG>\t[%-+8.4d]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%2.4d] -> 10000\n");
	mr = ft_printf("MINE>\t[%2.4d]\n", 10000);
	or = printf(   "ORIG>\t[%2.4d]\n", 10000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%10.1d] -> 10000\n");
	mr = ft_printf("MINE>\t[%10.1d]\n", 10000);
	or = printf(   "ORIG>\t[%10.1d]\n", 10000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%4.0d] -> 10000\n");
	mr = ft_printf("MINE>\t[%4.0d]\n", 10000);
	or = printf(   "ORIG>\t[%4.0d]\n", 10000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-5.4i] -> 10\n");
	mr = ft_printf("MINE>\t[%-5.4i]\n", 10);
	or = printf(   "ORIG>\t[%-5.4i]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%8.4i] -> 10\n");
	mr = ft_printf("MINE>\t[%8.4i]\n", 10);
	or = printf(   "ORIG>\t[%8.4i]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%5.4u] -> 10\n");
	mr = ft_printf("MINE>\t[%5.4u]\n", 10);
	or = printf(   "ORIG>\t[%5.4u]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-8.4u] -> 10\n");
	mr = ft_printf("MINE>\t[%-8.4u]\n", 10);
	or = printf(   "ORIG>\t[%-8.4u]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%5.4o] -> 10\n");
	mr = ft_printf("MINE>\t[%5.4o]\n", 10);
	or = printf(   "ORIG>\t[%5.4o]\n", 10);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-8.6o] -> 0456\n");
	mr = ft_printf("MINE>\t[%-8.6o]\n", 0456);
	or = printf(   "ORIG>\t[%-8.6o]\n", 0456);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%18.12o] -> 0777777777\n");
	mr = ft_printf("MINE>\t[%18.12o]\n", 0777777777);
	or = printf(   "ORIG>\t[%18.12o]\n", 0777777777);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%5.4p] -> 0xfaf\n");
	mr = ft_printf("MINE>\t[%5.4p]\n", 0xfaf);
	or = printf(   "ORIG>\t[%5.4p]\n", 0xfaf);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%8.4p] -> NULL\n");
	mr = ft_printf("MINE>\t[%8.4p]\n", NULL);
	or = printf(   "ORIG>\t[%8.4p]\n", NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-5.4p] -> 0xfaf\n");
	mr = ft_printf("MINE>\t[%-5.4p]\n", 0xfaf);
	or = printf(   "ORIG>\t[%-5.4p]\n", 0xfaf);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-8.4p] -> NULL\n");
	mr = ft_printf("MINE>\t[%-8.4p]\n", NULL);
	or = printf(   "ORIG>\t[%-8.4p]\n", NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%5.4x] -> 0xfaf\n");
	mr = ft_printf("MINE>\t[%5.4x]\n", 0xfaf);
	or = printf(   "ORIG>\t[%5.4x]\n", 0xfaf);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%5.4X] -> 0xfaf\n");
	mr = ft_printf("MINE>\t[%5.4X]\n", 0xfaf);
	or = printf(   "ORIG>\t[%5.4X]\n", 0xfaf);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-5.4X] -> 0xfaf\n");
	mr = ft_printf("MINE>\t[%-5.4X]\n", 0xfaf);
	or = printf(   "ORIG>\t[%-5.4X]\n", 0xfaf);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%5.4c] -> 'k'\n");
	mr = ft_printf("MINE>\t[%5.4c]\n", 'k');
	or = printf(   "ORIG>\t[%5.4c]\n", 'k');
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-5.4c] -> 'k'\n");
	mr = ft_printf("MINE>\t[%-5.4c]\n", 'k');
	or = printf(   "ORIG>\t[%-5.4c]\n", 'k');
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%5.4s] -> \"ok\"\n");
	mr = ft_printf("MINE>\t[%5.4s]\n", "ok");
	or = printf(   "ORIG>\t[%5.4s]\n", "ok");
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%-5.4s] -> \"ok\"\n");
	mr = ft_printf("MINE>\t[%-5.4s]\n", "ok");
	or = printf(   "ORIG>\t[%-5.4s]\n", "ok");
	num++;
	all += print_result(mr, or, __LINE__);

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m== End precision modifiers ==\033[0m\n\n");
	return (all);
}
