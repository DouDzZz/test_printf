/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.float.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/20 13:40:03 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/01 15:45:07 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_float(void)
{
	int
	mr, or, all, num;

	mr = or = all = num = 0;
	printf("\033[1;37m======== %%f ========\033[0m\n");

	fflush(stdout);
	printf("\nTEST>\t[%%f] -> 0.0\n");
	mr = ft_printf("MINE>\t[%f]\n", 0.0);
	fflush(stdout);
	or = printf("ORIG>\t[%f]\n", 0.0);
	all += print_result(mr, or, __LINE__);
	num++;
	
	printf("\nTEST>\t[%%.3f] -> 0.9995\n");
	mr = ft_printf("MINE>\t[%.3f]\n", 0.9995);
	fflush(stdout);
	or = printf("ORIG>\t[%.3f]\n", 0.9995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.4f] -> 0.9995\n");
	mr = ft_printf("MINE>\t[%.4f]\n", 0.9995);
	fflush(stdout);
	or = printf("ORIG>\t[%.4f]\n", 0.9995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.3f] -> 0.5555\n");
	mr = ft_printf("MINE>\t[%.3f]\n", 0.5555);
	fflush(stdout);
	or = printf("ORIG>\t[%.3f]\n", 0.5555);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.3f] -> 0.59995\n");
	mr = ft_printf("MINE>\t[%.3f]\n", 0.59995);
	fflush(stdout);
	or = printf("ORIG>\t[%.3f]\n", 0.59995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.4f] -> 0.59995\n");
	mr = ft_printf("MINE>\t[%.4f]\n", 0.59995);
	fflush(stdout);
	or = printf("ORIG>\t[%.4f]\n", 0.59995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.5f] -> 0.59995\n");
	mr = ft_printf("MINE>\t[%.5f]\n", 0.59995);
	fflush(stdout);
	or = printf("ORIG>\t[%.5f]\n", 0.59995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.6f] -> 0.59995\n");
	mr = ft_printf("MINE>\t[%.6f]\n", 0.59995);
	fflush(stdout);
	or = printf("ORIG>\t[%.6f]\n", 0.59995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.2f] -> 0.59995\n");
	mr = ft_printf("MINE>\t[%.2f]\n", 0.59995);
	fflush(stdout);
	or = printf("ORIG>\t[%.2f]\n", 0.59995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.2f] -> 0.559995\n");
	mr = ft_printf("MINE>\t[%.2f]\n", 0.559995);
	fflush(stdout);
	or = printf("ORIG>\t[%.2f]\n", 0.559995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.3f] -> 0.5559995\n");
	mr = ft_printf("MINE>\t[%.3f]\n", 0.5559995);
	fflush(stdout);
	or = printf("ORIG>\t[%.3f]\n", 0.5559995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.4f] -> 0.5559995\n");
	mr = ft_printf("MINE>\t[%.4f]\n", 0.5559995);
	fflush(stdout);
	or = printf("ORIG>\t[%.4f]\n", 0.5559995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.5f] -> 0.5559995\n");
	mr = ft_printf("MINE>\t[%.5f]\n", 0.5559995);
	fflush(stdout);
	or = printf("ORIG>\t[%.5f]\n", 0.5559995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.6f] -> 0.5559995\n");
	mr = ft_printf("MINE>\t[%.6f]\n", 0.5559995);
	fflush(stdout);
	or = printf("ORIG>\t[%.6f]\n", 0.5559995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.7f] -> 0.5559995\n");
	mr = ft_printf("MINE>\t[%.7f]\n", 0.5559995);
	fflush(stdout);
	or = printf("ORIG>\t[%.7f]\n", 0.5559995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%f] -> 127.123456789123456789123456789123456789\n");
	mr = ft_printf("MINE>\t[%f]\n", 127.123456789123456789123456789123456789);
	fflush(stdout);
	or = printf("ORIG>\t[%f]\n", 127.123456789123456789123456789123456789);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%f] -> 127.123456\n");
	mr = ft_printf("MINE>\t[%f]\n", 127.123456);
	fflush(stdout);
	or = printf("ORIG>\t[%f]\n", 127.123456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.f] -> 127.123456\n");
	mr = ft_printf("MINE>\t[%.f]\n", 127.123456);
	fflush(stdout);
	or = printf("ORIG>\t[%.f]\n", 127.123456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%.f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%.f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%20.f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%20.f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%20.f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%20.f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%20.f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%20.f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#20.f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%#20.f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%#20.f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#20f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%#20f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%#20f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%-20.f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%-20.f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%-20.f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%-20.f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%-20.f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%-20.f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#-20.f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%#-20.f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%#-20.f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#-20f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%#-20f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%#-20f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%% .f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[% .f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[% .f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%# .f] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%# .f]\n", 127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%# .f]\n", 127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% f -> 127.9996\n");
	mr = ft_printf("MINE>\t[% f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[% f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%#f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%#f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%# f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%# f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%# f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%-f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%-f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%+f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%+f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%+-f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%+-f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%0f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%0f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%0f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%0.15f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%0.15f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%0.15f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#0.15f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%#0.15f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%#0.15f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#+0.15f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%#+0.15f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%#+0.15f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%# +0.15f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%# +0.15f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%# +0.15f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%# +-0.15f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%# +-0.15f]\n",  127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%# +-0.15f]\n", 127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%.3f] -> -0.4995\n");
	mr = ft_printf("MINE>\t[%.3f]\n", -0.4995);
	fflush(stdout);
	or = printf("ORIG>\t[%.3f]\n", -0.4995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.3f] -> -0.0\n");
	mr = ft_printf("MINE>\t[%.3f]\n", -0.0);
	fflush(stdout);
	or = printf("ORIG>\t[%.3f]\n", -0.0);
	all += print_result(mr, or, __LINE__);
	num++;
  
	printf("\nTEST>\t[%%.3f] -> 0.0\n");
	mr = ft_printf("MINE>\t[%.3f]\n", 0.0);
	fflush(stdout);
	or = printf("ORIG>\t[%.3f]\n", 0.0);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.3f] -> -0.9995\n");
	mr = ft_printf("MINE>\t[%.3f]\n", -0.9995);
	fflush(stdout);
	or = printf("ORIG>\t[%.3f]\n", -0.9995);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%f] -> -127.123456789123456789123456789123456789\n");
	mr = ft_printf("MINE>\t[%f]\n", -127.123456789123456789123456789123456789);
	fflush(stdout);
	or = printf("ORIG>\t[%f]\n", -127.123456789123456789123456789123456789);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%f] -> -127.123456\n");
	mr = ft_printf("MINE>\t[%f]\n", -127.123456);
	fflush(stdout);
	or = printf("ORIG>\t[%f]\n", -127.123456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.f] -> -127.123456\n");
	mr = ft_printf("MINE>\t[%.f]\n", -127.123456);
	fflush(stdout);
	or = printf("ORIG>\t[%.f]\n", -127.123456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%.f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%.f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%20.f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%20.f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%20.f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%20.f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%20.f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%20.f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#20.f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%#20.f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%#20.f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#20f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%#20f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%#20f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%-20.f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%-20.f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%-20.f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%-20.f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%-20.f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%-20.f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#-20.f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%#-20.f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%#-20.f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#-20f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%#-20f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%#-20f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%% .f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[% .f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[% .f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%# .f] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%# .f]\n", -127.923456);
	fflush(stdout);
	or = printf("ORIG>\t[%# .f]\n", -127.923456);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% f -> -127.9996\n");
	mr = ft_printf("MINE>\t[% f]\n",  -127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[% f]\n", -127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#f -> -127.9996\n");
	mr = ft_printf("MINE>\t[%#f]\n",  -127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%#f]\n", -127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%# f -> -127.9996\n");
	mr = ft_printf("MINE>\t[%# f]\n",  -127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%# f]\n", -127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-f -> -127.9996\n");
	mr = ft_printf("MINE>\t[%-f]\n",  -127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%-f]\n", -127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+f -> -127.9996\n");
	mr = ft_printf("MINE>\t[%+f]\n",  -127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%+f]\n", -127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-f -> -127.9996\n");
	mr = ft_printf("MINE>\t[%+-f]\n",  -127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%+-f]\n", -127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%0f -> -127.9996\n");
	mr = ft_printf("MINE>\t[%0f]\n",  -127.9996);
	fflush(stdout);
	or = printf("ORIG>\t[%0f]\n", -127.9996);
	all += print_result(mr, or, __LINE__);
	num++;

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end %%f =====\033[0m\n\n");
	return (all);
}
