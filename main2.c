/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/04 18:12:31 by fldoucet          #+#    #+#             */
/*   Updated: 2019/03/01 18:40:20 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "/Users/edjubert/Documents/rendu/ft_printf/includes/ft_printf.h"

#include <limits.h>

int		main(void)
{
	// 		printf("\nTEST>\t%%jx -> 4294967296\n");
	// 		ft_printf("MINE>\t[%jx]", 4294967296);
	// 		printf("\n");
	// 		printf("ORIG>\t[%jx]", 4294967296);
	// 		printf("\n");

	// 		printf("\nTEST>\t%%jx -> -4294967295\n");
	// 		ft_printf("MINE>\t[%jx]", -4294967296);
	// 		printf("\n");
	// 		printf("ORIG>\t[%jx]", -4294967296);
	// 		printf("\n");

	// 		printf("\nTEST>\t%%jx -> -4294967297\n");
	// 		ft_printf("MINE>\t[%jx]", -4294967297);
	// 		printf("\n");
	// 		printf("ORIG>\t[%jx]", -4294967297);
	// 		printf("\n");

	// 	printf("\nTEST>\t%.2c -> NULL\n");
	// 	ft_printf("MINE>\t[%.2c]", NULL);
	// 	printf("\n");
	// 	printf("ORIG>\t[%.2c]", NULL);
	// 	printf("\n");

	// 	printf("\nTEST>\t%%2c -> 0\n");
	// 	ft_printf("MINE>\t[%2c]", 0);
	// 	printf("\n");
	// 	printf("ORIG>\t[%2c]", 0);
	// 	printf("\n");

	// 	printf("\nTEST>\t% c -> 0\n");
	// 	ft_printf("MINE>\t[% c]", 0);
	// 	printf("\n");
	// 	printf("ORIG>\t[% c]", 0);
	// 	printf("\n");

	// 	printf("\nTEST>\t%%0+5d -> 42\n");
	// 	ft_printf("MINE>\t[%0+5d]", 42);
	// 	printf("\n");
	// 	printf("ORIG>\t[%0+5d]", 42);
	// 	printf("\n");

	// printf("\nTEST>\t%%ld -> 9223372036854775807\n");
	// ft_printf("MINE>\t[%ld]", 9223372036854775807);
	// printf("\n");
	// printf("ORIG>\t[%ld]", 9223372036854775807);
	// printf("\n");

	// printf("\nTEST>\t%%lld -> 9223372036854775807\n");
	// ft_printf("MINE>\t[%lld]", 9223372036854775807);
	// printf("\n");
	// printf("ORIG>\t[%lld]", 9223372036854775807);
	// printf("\n");

	// printf("\nTEST>\t%%lld -> -2147483649\n");
	// ft_printf("MINE>\t[%lld]", -2147483649);
	// printf("\n");
	// printf("ORIG>\t[%lld]", -2147483649);
	// printf("\n");

	// printf("\nTEST>\t%%ld -> -2147483649\n");
	// ft_printf("MINE>\t[%ld]", -2147483649);
	// printf("\n");
	// printf("ORIG>\t[%ld]", -2147483649);
	// printf("\n");

	// printf("\nTEST>\t%%jd -> 9223372036854775807\n");
	// ft_printf("MINE>\t[%jd]", 9223372036854775807);
	// printf("\n");
	// printf("ORIG>\t[%jd]", 9223372036854775807);
	// printf("\n");

	// printf("\nTEST>\t%%jd -> -9223372036854775808\n");
	// ft_printf("MINE>\t[%jd]", -9223372036854775808);
	// printf("\n");
	// printf("ORIG>\t[%jd]", -9223372036854775808);
	// printf("\n");

	// printf("\nTEST>\t%%zd -> 4294967295\n");
	// ft_printf("MINE>\t[%zd]", 4294967295);
	// printf("\n");
	// printf("ORIG>\t[%zd]", 4294967295);
	// printf("\n");

	// printf("\nTEST>\t%%zd -> 4294967296\n");
	// ft_printf("MINE>\t[%zd]", 4294967296);
	// printf("\n");
	// printf("ORIG>\t[%zd]", 4294967296);
	// printf("\n");

	// printf("\nTEST>\t@moulitest: %.d %.0d -> 0, 0\n");
	// ft_printf("MINE>\t[@moulitest: [%.d] [%.0d]]", 0, 0);
	// printf("\n");
	// printf("ORIG>\t[@moulitest: [%.d] [%.0d]]", 0, 0);
	// printf("\n");
	
	// printf("\nTEST>\t%%u -> 4294967296\n");
	// ft_printf("MINE>\t[%u]", 4294967296);
	// printf("\n");
	// printf("ORIG>\t[%u]", 4294967296);
	// printf("\n");

	// printf("\nTEST>\t%%U -> 4294967295\n");
	// ft_printf("MINE>\t[%U]", 4294967295);
	// printf("\n");
	// printf("ORIG>\t[%U]", 4294967295);
	// printf("\n");

	// printf("\nTEST>\t%%hU -> 4294967296\n");
	// ft_printf("MINE>\t[%hU]", 4294967296);
	// printf("\n");
	// printf("ORIG>\t[%hU]", 4294967296);
	// printf("\n");

	// printf("\nTEST>\t%%U -> 4294967296\n");
	// ft_printf("MINE>\t[%U]", 4294967296);
	// printf("\n");
	// printf("ORIG>\t[%U]", 4294967296);
	// printf("\n");

	// printf("\nTEST>\t[%jx] -> 4294967296\n");
	// ft_printf("MINE>\t[%jx]\n", 4294967296);
	// printf("ORIG>\t[%jx]\n", 4294967296);

	// printf("\nTEST>\t[%%jx] -> -4294967296\n");
	// ft_printf("MINE>\t[%jx]\n", -4294967296);
	// printf("ORIG>\t[%jx]\n", -4294967296);

	// printf("\nTEST>\t[%%jx]\n", -4294967297);
	// ft_printf("MINE>\t[%jx]\n", -4294967297);
	// printf("ORIG>\t[%jx]\n", -4294967297);

	// printf("\nTEST>\t[%%ld] -> 2147483648\n");
	// ft_printf("MINE>\t[%ld]\n", -2147483648);
	// printf("ORIG>\t[%ld]\n", -2147483648);

	// printf("\nTEST>\t[%%ld] -> -2147483649\n");
	// ft_printf("MINE>\t[%ld]\n", -2147483649);
	// printf("ORIG>\t[%ld]\n", -2147483649);
	
	// printf("\nTEST>\t[%%jd] -> 9223372036854775807\n");
	// ft_printf("MINE>\t[%jd]\n", 9223372036854775807);
	// printf("ORIG>\t[%jd]\n", 9223372036854775807);

	// printf("\nTEST>\t[%%jd] -> -9223372036854775808\n");
	// ft_printf("MINE>\t[%jd]\n", -9223372036854775808);
	// printf("ORIG>\t[%jd]\n", -9223372036854775808);

	// printf("\nTEST>\t[%%zd] -> 4294967295\n");
	// ft_printf("MINE>\t[%zd]\n", 4294967295);
	// printf("ORIG>\t[%zd]\n", 4294967295);

	// printf("\nTEST>\t[%%zd] -> 4294967296\n");
	// ft_printf("MINE>\t[%zd]\n", 4294967296);
	// printf("ORIG>\t[%zd]\n", 4294967296);

	// printf("\nTEST>\t[%%zd] -> -1\n");
	// ft_printf("MINE>\t[%zd]\n", -1);
	// printf("ORIG>\t[%zd]\n", -1);

	// printf("\n[%%zd] ->  ULLONG_MAX\n");
	// ft_printf("[%zd]", ULLONG_MAX);
	// printf("\n");
	// printf("[%zd]", ULLONG_MAX);
	// printf("\n");

	// printf("\n[%%zd] ->  ULONG_MAX\n");
	// ft_printf("[%zd]", ULONG_MAX);
	// printf("\n");
	// printf("[%zd]", ULONG_MAX);
	// printf("\n");

	// printf("\n[%%zd] ->  LLONG_MAX\n");
	// ft_printf("[%zd]", LLONG_MAX);
	// printf("\n");
	// printf("[%zd]", LLONG_MAX);
	// printf("\n");

	// printf("\nTEST>\t[%%zd] ->  LONG_MAX\n");
	// ft_printf("MINE>\t[%zd]\n", LONG_MAX);
	// printf("ORIG>\t[%zd]\n", LONG_MAX);

	// printf("\nTEST>\t[%%zd] ->  INTMAX_MAX\n");
	// ft_printf("MINE>\t[%zd]\n", INTMAX_MAX);
	// printf("ORIG>\t[%zd]\n", INTMAX_MAX);

	// printf("\nTEST>\t[%%zd] -> 18446744073709551615\n");
	// ft_printf("MINE>\t[%zd]\n", 18446744073709551615);
	// printf("ORIG>\t[%zd]\n", 18446744073709551615);
	
	// printf("\nTEST>\t[@moulitest: [%%.d] [%%.0d]]\n", 0, 0);
	// ft_printf("MINE>\t[@moulitest: [%.d] [%.0d]]\n", 0, 0);
	// printf("ORIG>\t[@moulitest: [%.d] [%.0d]]\n", 0, 0);
	// fflush(stdout);

	// printf("\nTEST>\t[%ju] -> 4294967296\n");
	// ft_printf("MINE>\t[%ju]\n", 4294967296);
	// printf("ORIG>\t[%ju]\n", 4294967296);
 
	// printf("\nTEST>\t[%%u] -> 4294967296\n");
	// ft_printf("MINE>\t[%u]\n", 4294967296);
	// printf("ORIG>\t[%u]\n", 4294967296);

	// printf("\n");
	// printf("test %%#.o %%#.0o\n");
	// ft_printf("@moulitest: [%#.o] [%#.0o]", 0, 0);
	// printf("\n");
	// printf("@moulitest: [%#.o] [%#.0o]", 0, 0);
	// printf("\n");

	// printf("\ntest %%.o %%.0o\n");
	// ft_printf("@moulitest: [%.o] [%.0o]", 0, 0);
	// printf("\n");
	// printf("@moulitest: [%.o] [%.0o]", 0, 0);
	// printf("\n");

	// printf("\ntest %%#.x %%#.0x\n");
	// ft_printf("@moulitest: [%#.x] [%#.0x]", 0, 0);
	// printf("\n");
	// printf("@moulitest: [%#.x] [%#.0x]", 0, 0);
	// printf("\n");
	
	// printf("\ntest %%.x %%.0x\n");
	// ft_printf("@moulitest: [%.x] [%.0x]", 0, 0);
	// printf("\n");
	// printf("@moulitest: [%.x] [%.0x]", 0, 0);
	// printf("\n");

	// printf("\n@moulitest: [%%.10d] -> -42\n");
	// ft_printf("@moulitest: [%.10d]", -42);
	// printf("\n");
	// printf("@moulitest: [%.10d]", -42);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%ld] -> -2147483648\n");
	// ft_printf("@moulitest: [%ld]", -2147483648);
	// printf("\n");
	// printf("@moulitest: [%ld]", -2147483648);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%ld] -> -9223372036854775808\n");
	// ft_printf("@moulitest: [%ld]", -9223372036854775808);
	// printf("\n");
	// printf("@moulitest: [%ld]", -9223372036854775808);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%lld] -> -9223372036854775808\n");
	// ft_printf("@moulitest: [%lld]", -9223372036854775808);
	// printf("\n");
	// printf("@moulitest: [%lld]", -9223372036854775808);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%ld] -> 2147483647\n");
	// ft_printf("@moulitest: [%ld]", 2147483647);
	// printf("\n");
	// printf("@moulitest: [%ld]", 2147483647);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%ld] -> 9223372036854775807\n");
	// ft_printf("@moulitest: [%ld]", 9223372036854775807);
	// printf("\n");
	// printf("@moulitest: [%ld]", 9223372036854775807);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%lld] -> 9223372036854775807\n");
	// ft_printf("@moulitest: [%lld]", 9223372036854775807);
	// printf("\n");
	// printf("@moulitest: [%lld]", 9223372036854775807);
	// printf("\n");

	// ft_printf("{red}%T{eoc}\n", "SEGTEST");

	// printf("%%hu, %%hu -> 0, USHRT_MAX\n");
	// ft_printf("MINE>\t[%hu], [%hu]", 0, USHRT_MAX);
	// printf("ORIG>\t[%hu], [%hu]", 0, USHRT_MAX);

	// ft_printf("\n\n{red}%T{eoc}\n", "END SEGTEST");

	// printf("\nTEST>\t[%%10.5d] -> 4242\n");
	// ft_printf("[%10.5d]\n", 4242);
	// printf("[%10.5d]\n", 4242);

	// printf("\nTEST>\t[%%-10.5d] -> 4242\n");
	// ft_printf("[%-10.5d]\n", 4242);
	// printf("[%-10.5d]\n", 4242);

	// printf("\nTEST>\t[%%03.2d] -> 0\n");
	// ft_printf("[%03.2d]\n", 0);
	// printf("[%03.2d]\n", 0);

	// printf("\nTEST>\t[%%+.5d] -> -4242\n");
	// ft_printf("[%+.5d]\n", -4242);
	// printf("[%+.5d]\n", -4242);

	// printf("\nTEST>\t[%%+.5d] -> 4242\n");
	// ft_printf("[%+.5d]\n", 4242);
	// printf("[%+.5d]\n", 4242);
	
	// printf("\nTEST>\t[%% 10.5d] -> 4242\n");
	// ft_printf("[% 10.5d]\n", 4242);
	// printf("[% 10.5d]\n", 4242);
	
	// printf("\nTEST>\t[%% 10.4d] -> 4242\n");
	// ft_printf("[% 10.4d]\n", 4242);
	// printf("[% 10.4d]\n", 4242);

	// printf("\nTEST>\t[@moulitest: [%%.10d] -> -42\n");
	// ft_printf("@moulitest: [%.10d]\n", -42);
	// printf("@moulitest: [%.10d]\n", -42);

	// printf("\nTEST>\t[@moulitest: [%%.3d] -> -42\n");
	// ft_printf("@moulitest: [%.3d]\n", -42);
	// printf("@moulitest: [%.3d]\n", -42);

	// ft_printf("{red}%T{eoc}", "TEST X");

	// 	printf("\n@moulitest: [%%.10x] -> -42\n");
	// 	ft_printf("@moulitest: [%.10x]", -42);
	// 	printf("\n");
	// 	printf("@moulitest: [%.10x]", -42);
	// 	printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%lx] -> -2147483648\n");
	// ft_printf("@moulitest: [%lx]", -2147483648);
	// printf("\n");
	// printf("@moulitest: [%lx]", -2147483648);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%lx] -> -9223372036854775808\n");
	// ft_printf("@moulitest: [%lx]", -9223372036854775808);
	// printf("\n");
	// printf("@moulitest: [%lx]", -9223372036854775808);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%llx] -> -9223372036854775808\n");
	// ft_printf("@moulitest: [%llx]", -9223372036854775808);
	// printf("\n");
	// printf("@moulitest: [%llx]", -9223372036854775808);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%lx] -> 2147483647\n");
	// ft_printf("@moulitest: [%lx]", 2147483647);
	// printf("\n");
	// printf("@moulitest: [%lx]", 2147483647);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%lx] -> 9223372036854775807\n");
	// ft_printf("@moulitest: [%lx]", 9223372036854775807);
	// printf("\n");
	// printf("@moulitest: [%lx]", 9223372036854775807);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%llx] -> 9223372036854775807\n");
	// ft_printf("@moulitest: [%llx]", 9223372036854775807);
	// printf("\n");
	// printf("@moulitest: [%llx]", 9223372036854775807);
	// printf("\n");
	// // ft_printf("%hu, %hu", 0, USHRT_MAX);

	// printf("\nTEST>\t[%%10.5x] -> 4242\n");
	// ft_printf("[%10.5x]\n", 4242);
	// printf("[%10.5x]\n", 4242);

	// printf("\nTEST>\t[%%-10.5x] -> 4242\n");
	// ft_printf("[%-10.5x]\n", 4242);
	// printf("[%-10.5x]\n", 4242);

	// printf("\nTEST>\t[%%03.2x] -> 0\n");
	// ft_printf("[%03.2x]\n", 0);
	// printf("[%03.2x]\n", 0);

	// printf("\nTEST>\t[%%03.2x] -> 127\n");
	// ft_printf("[%03.2x]\n", 127);
	// printf("[%03.2x]\n", 127);

	// printf("\nTEST>\t[%%03.2x] -> -127\n");
	// ft_printf("[%03.2x]\n", -127);
	// printf("[%03.2x]\n", -127);

	// printf("\nTEST>\t[%%+.5x] -> -4242\n");
	// ft_printf("[%+.5x]\n", -4242);
	// printf("[%+.5x]\n", -4242);

	// printf("\nTEST>\t[%%+.5x] -> 4242\n");
	// ft_printf("[%+.5x]\n", 4242);
	// printf("[%+.5x]\n", 4242);
	
	// printf("\nTEST>\t[%% 10.5x] -> 4242\n");
	// ft_printf("[% 10.5x]\n", 4242);
	// printf("[% 10.5x]\n", 4242);
	
	// printf("\nTEST>\t[%% 10.4x] -> 4242\n");
	// ft_printf("[% 10.4x]\n", 4242);
	// printf("[% 10.4x]\n", 4242);

	// printf("\nTEST>\t[@moulitest: [%%.10x] -> -42\n");
	// ft_printf("@moulitest: [%.10x]\n", -42);
	// printf("@moulitest: [%.10x]\n", -42);

	// printf("\nTEST>\t[@moulitest: [%%.3x] -> -42\n");
	// ft_printf("@moulitest: [%.3x]\n", -42);
	// printf("@moulitest: [%.3x]\n", -42);

	// ft_printf("{red}%T{eoc}", "TEST o");

	// 	printf("\n@moulitest: [%%.10o] -> -42\n");
	// 	ft_printf("@moulitest: [%.10o]", -42);
	// 	printf("\n");
	// 	printf("@moulitest: [%.10o]", -42);
	// 	printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%lo] -> -2147483648\n");
	// ft_printf("@moulitest: [%lo]", -2147483648);
	// printf("\n");
	// printf("@moulitest: [%lo]", -2147483648);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%lo] -> -9223372036854775808\n");
	// ft_printf("@moulitest: [%lo]", -9223372036854775808);
	// printf("\n");
	// printf("@moulitest: [%lo]", -9223372036854775808);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%llo] -> -9223372036854775808\n");
	// ft_printf("@moulitest: [%llo]", -9223372036854775808);
	// printf("\n");
	// printf("@moulitest: [%llo]", -9223372036854775808);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%lo] -> 2147483647\n");
	// ft_printf("@moulitest: [%lo]", 2147483647);
	// printf("\n");
	// printf("@moulitest: [%lo]", 2147483647);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%lo] -> 9223372036854775807\n");
	// ft_printf("@moulitest: [%lo]", 9223372036854775807);
	// printf("\n");
	// printf("@moulitest: [%lo]", 9223372036854775807);
	// printf("\n");

	// fflush(stdout);
	// printf("\n@moulitest: [%%llo] -> 9223372036854775807\n");
	// ft_printf("@moulitest: [%llo]", 9223372036854775807);
	// printf("\n");
	// printf("@moulitest: [%llo]", 9223372036854775807);
	// printf("\n");
	// // ft_printf("%hu, %hu", 0, USHRT_MAX);

	// printf("\nTEST>\t[%%10.5o] -> 4242\n");
	// ft_printf("[%10.5o]\n", 4242);
	// printf("[%10.5o]\n", 4242);

	// printf("\nTEST>\t[%%-10.5o] -> 4242\n");
	// ft_printf("[%-10.5o]\n", 4242);
	// printf("[%-10.5o]\n", 4242);

	// printf("\nTEST>\t[%%03.2o] -> 0\n");
	// ft_printf("[%03.2o]\n", 0);
	// printf("[%03.2o]\n", 0);

	// printf("\nTEST>\t[%%03.2o] -> 127\n");
	// ft_printf("[%03.2o]\n", 127);
	// printf("[%03.2o]\n", 127);

	// printf("\nTEST>\t[%%03.2o] -> -127\n");
	// ft_printf("[%03.2o]\n", -127);
	// printf("[%03.2o]\n", -127);

	// printf("\nTEST>\t[%%03.2lo] -> 0\n");
	// ft_printf("[%03.2o]\n", 0);
	// printf("[%03.2o]\n", 0);

	// printf("\nTEST>\t[%%03.2lo] -> 127\n");
	// ft_printf("[%03.2o]\n", 127);
	// printf("[%03.2o]\n", 127);

	// printf("\nTEST>\t[%%03.2lo] -> -127\n");
	// ft_printf("[%03.2o]\n", -127);
	// printf("[%03.2o]\n", -127);

	// printf("\nTEST>\t[%%03.2llo] -> 0\n");
	// ft_printf("[%03.2o]\n", 0);
	// printf("[%03.2o]\n", 0);

	// printf("\nTEST>\t[%%03.2llo] -> 127\n");
	// ft_printf("[%03.2o]\n", 127);
	// printf("[%03.2o]\n", 127);

	// printf("\nTEST>\t[%%03.2llo] -> -127\n");
	// ft_printf("[%03.2o]\n", -127);
	// printf("[%03.2o]\n", -127);

	// printf("\nTEST>\t[%%+.5o] -> -4242\n");
	// ft_printf("[%+.5o]\n", -4242);
	// printf("[%+.5o]\n", -4242);

	// printf("\nTEST>\t[%%+.5o] -> 4242\n");
	// ft_printf("[%+.5o]\n", 4242);
	// printf("[%+.5o]\n", 4242);
	
	// printf("\nTEST>\t[%% 10.5o] -> 4242\n");
	// ft_printf("[% 10.5o]\n", 4242);
	// printf("[% 10.5o]\n", 4242);
	
	// printf("\nTEST>\t[%% 10.4o] -> 4242\n");
	// ft_printf("[% 10.4o]\n", 4242);
	// printf("[% 10.4o]\n", 4242);

	// printf("\nTEST>\t[@moulitest: [%%.10o] -> -42\n");
	// ft_printf("@moulitest: [%.10o]\n", -42);
	// printf("@moulitest: [%.10o]\n", -42);

	// printf("\nTEST>\t[@moulitest: [%%.3o] -> -42\n");
	// ft_printf("@moulitest: [%.3o]\n", -42);
	// printf("@moulitest: [%.3o]\n", -42);

	// printf("\nTEST>\t[%%Lf] -> 0.0\n");
	// ft_printf("MINE>\t[%Lf]\n", 0.0L);
	// printf("ORIG>\t[%Lf]\n", 0.0L);
	
	// printf("\nTEST>\t[%%.3Lf] -> 0.9995\n");
	// ft_printf("MINE>\t[%.3Lf]\n", 0.9995L);
	// printf("ORIG>\t[%.3Lf]\n", 0.9995L);

	// printf("\nTEST>\t[%%.4Lf] -> 0.9995\n");
	// ft_printf("MINE>\t[%.4Lf]\n", 0.9995L);
	// printf("ORIG>\t[%.4Lf]\n", 0.9995L);

	// printf("\nTEST>\t[%%.3Lf] -> 0.5555\n");
	// ft_printf("MINE>\t[%.3Lf]\n", 0.5555L);
	// printf("ORIG>\t[%.3Lf]\n", 0.5555L);

	// printf("\nTEST>\t[%%.3Lf] -> 0.59995\n");
	// ft_printf("MINE>\t[%.3Lf]\n", 0.59995L);
	// printf("ORIG>\t[%.3Lf]\n", 0.59995L);

	// printf("\nTEST>\t[%%.4Lf] -> 0.59995\n");
	// ft_printf("MINE>\t[%.4Lf]\n", 0.59995L);
	// printf("ORIG>\t[%.4Lf]\n", 0.59995L);

	// printf("\nTEST>\t[%%.5Lf] -> 0.59995\n");
	// ft_printf("MINE>\t[%.5Lf]\n", 0.59995L);
	// printf("ORIG>\t[%.5Lf]\n", 0.59995L);

	// printf("\nTEST>\t[%%.6Lf] -> 0.59995\n");
	// ft_printf("MINE>\t[%.6Lf]\n", 0.59995L);
	// printf("ORIG>\t[%.6Lf]\n", 0.59995L);

	// printf("\nTEST>\t[%%.2Lf] -> 0.59995\n");
	// ft_printf("MINE>\t[%.2Lf]\n", 0.59995L);
	// printf("ORIG>\t[%.2Lf]\n", 0.59995L);

	// printf("\nTEST>\t[%%.2Lf] -> 0.559995\n");
	// ft_printf("MINE>\t[%.2Lf]\n", 0.559995L);
	// printf("ORIG>\t[%.2Lf]\n", 0.559995L);

	// printf("\nTEST>\t[%%.3Lf] -> 0.5559995\n");
	// ft_printf("MINE>\t[%.3Lf]\n", 0.5559995L);
	// printf("ORIG>\t[%.3Lf]\n", 0.5559995L);

	// printf("\nTEST>\t[%%.4Lf] -> 0.5559995\n");
	// ft_printf("MINE>\t[%.4Lf]\n", 0.5559995L);
	// printf("ORIG>\t[%.4Lf]\n", 0.5559995L);

	// printf("\nTEST>\t[%%.5Lf] -> 0.5559995\n");
	// ft_printf("MINE>\t[%.5Lf]\n", 0.5559995L);
	// printf("ORIG>\t[%.5Lf]\n", 0.5559995L);

	// printf("\nTEST>\t[%%.6Lf] -> 0.5559995\n");
	// ft_printf("MINE>\t[%.6Lf]\n", 0.5559995L);
	// printf("ORIG>\t[%.6Lf]\n", 0.5559995L);

	// printf("\nTEST>\t[%%.7Lf] -> 0.5559995\n");
	// ft_printf("MINE>\t[%.7Lf]\n", 0.5559995L);
	// printf("ORIG>\t[%.7Lf]\n", 0.5559995L);

	// printf("\nTEST>\t[%%Lf] -> 127.123456789123456789123456789123456789\n");
	// ft_printf("MINE>\t[%Lf]\n", 127.123456789123456789123456789123456789L);
	// printf("ORIG>\t[%Lf]\n", 127.123456789123456789123456789123456789L);

	// printf("\nTEST>\t[%%Lf] -> 127.123456\n");
	// ft_printf("MINE>\t[%Lf]\n", 127.123456L);
	// printf("ORIG>\t[%Lf]\n", 127.123456L);

	// printf("\nTEST>\t[%%.Lf] -> 127.123456\n");
	// ft_printf("MINE>\t[%.Lf]\n", 127.123456L);
	// printf("ORIG>\t[%.Lf]\n", 127.123456L);

	// printf("\nTEST>\t[%%.Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[%.Lf]\n", 127.923456L);
	// printf("ORIG>\t[%.Lf]\n", 127.923456L);

	// printf("\nTEST>\t[%%20.Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[%20.Lf]\n", 127.923456L);
	// printf("ORIG>\t[%20.Lf]\n", 127.923456L);

	// printf("\nTEST>\t[%%20.Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[%20.Lf]\n", 127.923456L);
	// printf("ORIG>\t[%20.Lf]\n", 127.923456L);

	// printf("\nTEST>\t[%%#20.Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[%#20.Lf]\n", 127.923456L);
	// printf("ORIG>\t[%#20.Lf]\n", 127.923456L);

	// printf("\nTEST>\t[%%#20Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[%#20Lf]\n", 127.923456L);
	// printf("ORIG>\t[%#20Lf]\n", 127.923456L);

	// printf("\nTEST>\t[%%-20.Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[%-20.Lf]\n", 127.923456L);
	// printf("ORIG>\t[%-20.Lf]\n", 127.923456L);

	// printf("\nTEST>\t[%%-20.Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[%-20.Lf]\n", 127.923456L);
	// printf("ORIG>\t[%-20.Lf]\n", 127.923456L);

	// printf("\nTEST>\t[%%#-20.Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[%#-20.Lf]\n", 127.923456L);
	// printf("ORIG>\t[%#-20.Lf]\n", 127.923456L);

	// printf("\nTEST>\t[%%#-20Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[%#-20Lf]\n", 127.923456L);
	// printf("ORIG>\t[%#-20Lf]\n", 127.923456L);

	// printf("\nTEST>\t[%% .Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[% .Lf]\n", 127.923456L);
	// printf("ORIG>\t[% .Lf]\n", 127.923456L);

	// printf("\nTEST>\t[%%# .Lf] -> 127.923456\n");
	// ft_printf("MINE>\t[%# .Lf]\n", 127.923456L);
	// printf("ORIG>\t[%# .Lf]\n", 127.923456L);

	// printf("\nTEST: %% f -> 127.9996\n");
	// ft_printf("MINE>\t[% Lf]\n",  127.9996L);
	// printf("ORIG>\t[% Lf]\n", 127.9996L);

	// printf("\nTEST: %%#f -> 127.9996\n");
	// ft_printf("MINE>\t[%#Lf]\n",  127.9996L);
	// printf("ORIG>\t[%#Lf]\n", 127.9996L);

	// printf("\nTEST: %%# f -> 127.9996\n");
	// ft_printf("MINE>\t[%# Lf]\n",  127.9996L);
	// printf("ORIG>\t[%# Lf]\n", 127.9996L);

	// printf("\nTEST: %%-f -> 127.9996\n");
	// ft_printf("MINE>\t[%-Lf]\n",  127.9996L);
	// printf("ORIG>\t[%-Lf]\n", 127.9996L);

	// printf("\nTEST: %%+f -> 127.9996\n");
	// ft_printf("MINE>\t[%+Lf]\n",  127.9996L);
	// printf("ORIG>\t[%+Lf]\n", 127.9996L);

	// printf("\nTEST: %%+-f -> 127.9996\n");
	// ft_printf("MINE>\t[%+-Lf]\n",  127.9996L);
	// printf("ORIG>\t[%+-Lf]\n", 127.9996L);

	// printf("\nTEST: %%0f -> 127.9996\n");
	// ft_printf("MINE>\t[%0Lf]\n",  127.9996L);
	// printf("ORIG>\t[%0Lf]\n", 127.9996L);

	// printf("\nTEST: %%0.15f -> 127.9996\n");
	// ft_printf("MINE>\t[%0.14Lf]\n",  127.9996L);
	// printf("ORIG>\t[%0.14Lf]\n", 127.9996L);

	// printf("\nTEST: %%#0.15f -> 127.9996\n");
	// ft_printf("MINE>\t[%#0.14Lf]\n",  127.9996L);
	// printf("ORIG>\t[%#0.14Lf]\n", 127.9996L);

	// printf("\nTEST: %%#+0.15f -> 127.9996\n");
	// ft_printf("MINE>\t[%#+0.14Lf]\n",  127.9996L);
	// printf("ORIG>\t[%#+0.14Lf]\n", 127.9996L);

	// printf("\nTEST: %%# +0.15f -> 127.9996\n");
	// ft_printf("MINE>\t[%# +0.14Lf]\n",  127.9996L);
	// printf("ORIG>\t[%# +0.14Lf]\n", 127.9996L);

	// printf("\nTEST: %%# +-0.15f -> 127.9996\n");
	// ft_printf("MINE>\t[%# +-0.14Lf]\n",  127.9996L);
	// printf("ORIG>\t[%# +-0.14Lf]\n", 127.9996L);

	// printf("TEST>\t[%%.3Lf] -> -0.4995\n");
	// ft_printf("MINE>\t[%.3Lf]\n", -0.4995L);
	// printf("ORIG>\t[%.3Lf]\n", -0.4995L);

	// printf("\nTEST>\t[%%.3Lf] -> -0.0\n");
	// ft_printf("MINE>\t[%.3Lf]\n", -0.0L);
	// printf("ORIG>\t[%.3Lf]\n", -0.0L);
  
	// printf("\nTEST>\t[%%.3Lf] -> 0.0\n");
	// ft_printf("MINE>\t[%.3Lf]\n", 0.0L);
	// printf("ORIG>\t[%.3Lf]\n", 0.0L);

	// printf("\nTEST>\t[%%.3Lf] -> -0.9995\n");
	// ft_printf("MINE>\t[%.3Lf]\n", -0.9995L);
	// printf("ORIG>\t[%.3Lf]\n", -0.9995L);

	// printf("\nTEST>\t[%%Lf] -> -127.123456789123456789123456789123456789\n");
	// ft_printf("MINE>\t[%Lf]\n", -127.123456789123456789123456789123456789L);
	// printf("ORIG>\t[%Lf]\n", -127.123456789123456789123456789123456789L);

	// printf("\nTEST>\t[%%Lf] -> -127.123456\n");
	// ft_printf("MINE>\t[%Lf]\n", -127.123456L);
	// printf("ORIG>\t[%Lf]\n", -127.123456L);

	// printf("\nTEST>\t[%%.Lf] -> -127.123456\n");
	// ft_printf("MINE>\t[%.Lf]\n", -127.123456L);
	// printf("ORIG>\t[%.Lf]\n", -127.123456L);

	// printf("\nTEST>\t[%%.Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[%.Lf]\n", -127.923456L);
	// printf("ORIG>\t[%.Lf]\n", -127.923456L);

	// printf("\nTEST>\t[%%20.Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[%20.Lf]\n", -127.923456L);
	// printf("ORIG>\t[%20.Lf]\n", -127.923456L);

	// printf("\nTEST>\t[%%20.Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[%20.Lf]\n", -127.923456L);
	// printf("ORIG>\t[%20.Lf]\n", -127.923456L);

	// printf("\nTEST>\t[%%#20.Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[%#20.Lf]\n", -127.923456L);
	// printf("ORIG>\t[%#20.Lf]\n", -127.923456L);

	// printf("\nTEST>\t[%%#20Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[%#20Lf]\n", -127.923456L);
	// printf("ORIG>\t[%#20Lf]\n", -127.923456L);

	// printf("\nTEST>\t[%%-20.Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[%-20.Lf]\n", -127.923456L);
	// printf("ORIG>\t[%-20.Lf]\n", -127.923456L);

	// printf("\nTEST>\t[%%-20.Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[%-20.Lf]\n", -127.923456L);
	// printf("ORIG>\t[%-20.Lf]\n", -127.923456L);

	// printf("\nTEST>\t[%%#-20.Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[%#-20.Lf]\n", -127.923456L);
	// printf("ORIG>\t[%#-20.Lf]\n", -127.923456L);

	// printf("\nTEST>\t[%%#-20Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[%#-20Lf]\n", -127.923456L);
	// printf("ORIG>\t[%#-20Lf]\n", -127.923456L);

	// printf("\nTEST>\t[%% .Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[% .Lf]\n", -127.923456L);
	// printf("ORIG>\t[% .Lf]\n", -127.923456L);

	// printf("\nTEST>\t[%%# .Lf] -> -127.923456\n");
	// ft_printf("MINE>\t[%# .Lf]\n", -127.923456L);
	// printf("ORIG>\t[%# .Lf]\n", -127.923456L);

	// printf("\nTEST: %% Lf -> -127.9996\n");
	// ft_printf("MINE>\t[% Lf]\n",  -127.9996L);
	// printf("ORIG>\t[% Lf]\n", -127.9996L);

	// printf("\nTEST: %%#Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%#Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%#Lf]\n", -127.9996L);

	// printf("\nTEST: %%# Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%# Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%# Lf]\n", -127.9996L);

	// printf("\nTEST: %%-Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%-Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%-Lf]\n", -127.9996L);

	// printf("\nTEST: %%+Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%+Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%+Lf]\n", -127.9996L);

	// printf("\nTEST: %%+-Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%+-Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%+-Lf]\n", -127.9996L);

	// printf("\nTEST: %%0Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%0Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%0Lf]\n", -127.9996L);

	// printf("\nTEST: %%0.14Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%0.14Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%0.14Lf]\n", -127.9996L);

	// printf("\nTEST: %%#0.14Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%#0.14Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%#0.14Lf]\n", -127.9996L);

	// printf("\nTEST: %%#+0.14Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%#+0.14Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%#+0.14Lf]\n", -127.9996L);

	// printf("\nTEST: %%# +0.14Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%# +0.14Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%# +0.14Lf]\n", -127.9996L);

	// printf("\nTEST: %%# +-0.14Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%# +-0.14Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%# +-0.14Lf]\n", -127.9996L);

	// printf("\nTEST: %%# +-0.14Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%# +-0.14Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%# +-0.14Lf]\n", -127.9996L);

	// printf("\nTEST: %%# +-0.16Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%# +-0.16Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%# +-0.16Lf]\n", -127.9996L);

	// printf("\nTEST: %%# +-0.17Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%# +-0.17Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%# +-0.17Lf]\n", -127.9996L);

	// printf("\nTEST: %%# +-0.18Lf -> -127.9996\n");
	// ft_printf("MINE>\t[%# +-0.18Lf]\n",  -127.9996L);
	// printf("ORIG>\t[%# +-0.18Lf]\n", -127.9996L);

	
	// printf("\n");
	// printf("%%.10d -> 4242");
	// printf("\n");
	// ft_printf("%.10d", 4242);
	// printf("\n");
	// printf("%.10d", 4242);

	ft_printf("[%.u], [%.0u]\n", 0, 0);
	printf("[%.u], [%.0u]\n\n", 0, 0);

	ft_printf("{%f}{%F}\n", 1.42, 1.42);
	printf("{%f}{%F}\n", 1.42, 1.42);
	return (0);
}
