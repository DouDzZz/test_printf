/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.crashtest.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 15:10:14 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/27 11:07:39 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_crashtest(void)
{
	int
	mr, or, all, num;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;

	printf("\033[1;37m==== Crashtest zone ===\033[0m\n");

	printf("TEST>\t[] -> 25, 16215\n");
	mr = ft_printf("MINE>\t[]\n", 25, 16215);
	or = printf("ORIG>\t[]\n", 25, 16215);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%d]\n");
	mr = ft_printf("MINE>\t[%d]\n");
	or = printf("ORIG>\t[%d]\n");
	num++;
	all += print_result(mr, or, __LINE__);

	printf("MINE>\t[-10+d s : {%%+-10d} {%%s}] -> 12345, \"argl\"\n");
	mr = ft_printf("MINE>\t[-10+d s : {%+-10d} {%s}]\n", 12345, "argl");
	or = printf(   "ORIG>\t[-10+d s : {%+-10d} {%s}]\n", 12345, "argl");
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%] -> 25, 16215\n");
	mr = ft_printf("MINE>\t[%]\n", 25, 16215);
	or = printf("ORIG>\t[%]\n", 25, 16215);
	printf("\033[0m");
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%%%] -> 25, 16215\n");
	mr = ft_printf("MINE>\t[%%]\n", 25, 16215);
	or = printf("ORIG>\t[%%]\n", 25, 16215);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%%%%%] -> 25, 16215\n");
	mr = ft_printf("MINE>\t[%%%]\n", 25, 16215);
	or = printf("ORIG>\t[%%%]\n", 25, 16215);
	printf("\033[0m");
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[30%%%%] -> 25, 16215\n");
	mr = ft_printf("MINE>\t[30%%]\n", 25, 16215);
	or = printf("ORIG>\t[30%%]\n", 25, 16215);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TESST>\t[%%30%%] -> 25, 16215\n");
	mr = ft_printf("MINE>\t[%30%]\n", 25, 16215);
	or = printf("ORIG>\t[%30%]\n", 25, 16215);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%%%%%%%] -> 25, 16215\n");
	mr = ft_printf("MINE>\t[%%%%]\n", 25, 16215);
	or = printf("ORIG>\t[%%%%]\n", 25, 16215);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%{after] -> 25, 16215\n");
	mr = ft_printf("MINE>\t[%{after]\n", 25, 16215);
	or = printf("ORIG>\t[%{after]\n", 25, 16215);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hh{after] -> 25, 16215\n");
	mr = ft_printf("MINE>\t[%hh{after]\n", 25, 16215);
	or = printf("ORIG>\t[%hh{after]\n", 25, 16215);
	num++;
	all += print_result(mr, or, __LINE__);

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m== End Crashtest zone ==\033[0m\n\n");
	return (all);
}
