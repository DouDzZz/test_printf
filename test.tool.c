/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 11:32:03 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/18 14:29:44 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int		print_result(int mr, int or, int line)
{
	static int c = 0;
	printf("\033[33m[%02d]\033[0m\tMINE = %i - ORIG = %i", c, mr, or);
	if (mr == or)
		printf("\t\033[32m✔\033[0m");
	else
		printf("\t\033[31m✘ - (failed between lines %3i and %3i)\033[0m",
			   line - 3, line);
	printf("\n---------------\n");
	c++;
	return (mr == or ? 1 : 0);
}
