/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.width.parameters.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 15:05:20 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/20 11:53:25 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_width_parameters(void)
{
	int
	mr, or, all, num;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;

	printf("\033[1;37m==== Width parameter ===\033[0m\n");

	mr = ft_printf("MINE>\t[%*d]\n", 10, 999);
	or = printf(   "ORIG>\t[%*d]\n", 10, 999);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%*d]\n", -10, 999);
	or = printf(   "ORIG>\t[%*d]\n", -10, 999);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%*d]\n", 999);
	or = printf(   "ORIG>\t[%*d]\n", 999);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%*i]\n", 10, 123);
	or = printf(   "ORIG>\t[%*i]\n", 10, 123);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%*s]\n", 10, "chamo");
	or = printf(   "ORIG>\t[%*s]\n", 10, "chamo");
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%*c]\n", 8, 'c');
	or = printf(   "ORIG>\t[%*c]\n", 8, 'c');
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%-*c]\n", 8, 'k');
	or = printf(   "ORIG>\t[%-*c]\n", 8, 'k');
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[% '#*p]\n", 18, &or);
	or = printf(   "ORIG>\t[% '#*p]\n", 18, &or);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%#'-*X]\n", 18, 167715);
	or = printf(   "ORIG>\t[%#'-*X]\n", 18, 167715);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%#+*x]\n", 25, 1777215);
	or = printf("ORIG>\t[%#+*x]\n", 25, 1777215);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%#-*X]\n", 25, 16215);
	or = printf("ORIG>\t[%#-*X]\n", 25, 16215);
	num++;
	all += print_result(mr, or, __LINE__);


	printf("\033[1;37m== End Width parameter ==\033[0m\n\n");

	printf("\033[1;37m= End Numbered width parameter =\033[0m\n");

	mr = ft_printf("MINE>\t[%*2$d]\n", 10, 40);
	or = printf(   "ORIG>\t[%*2$d]\n", 10, 40);
	num++;
	all += print_result(mr, or, __LINE__);

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m== End Width parameter ==\033[0m\n\n");
	return (all);
}
