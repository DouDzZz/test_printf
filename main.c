/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 11:30:12 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/01 17:29:26 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

static int	is_num(char *str)
{
	if (*str == '-' || *str == '+')
		++str;
	if (*str <= '9' && *str >= '0')
		return (1);
	return (0);
}

static int	print_usage(void)
{
	printf("Usage:\ttest\t[[-avhrR]|test_nb]\n");
	printf("\t-v\t- verbose mode, print all outputs\n");
	printf("\t-a\t- all tests\n");
	printf("\t-h\t- print usages\n");
	printf("\t0\t- all test\n");
	printf("\t1\t- test_digit\n");
	printf("\t2\t- test_unsigned\n");
	printf("\t3\t- test_strings\n");
	printf("\t4\t- test_chars\n");
	printf("\t5\t- test_octal\n");
	printf("\t6\t- test_hexadecimal\n");
	printf("\t7\t- test_hexadecimal_hard\n");
	printf("\t8\t- test_memory\n");
	printf("\t9\t- test_width_parameters\n");
	printf("\t10\t- test_crashtest\n");
	printf("\t11\t- test_modifiers\n");
	printf("\t12\t- test_precision\n");
	printf("\t13\t- test_float\n");
	printf("\t14\t- test_float long wrong\n");
	printf("\t15\t- test_float long\n");
	printf("\t16\t- test_float hard\n");
	printf("\t17\t- test filechecker\n");
	printf("\t18\t- test binary\n");
	return (2);
}

int			main(int ac, char **av)
{
	int
	passed_tests, dig, uns, str, cha, oct, hexa, hexahard, mem, wid, crash, mod, pre, flo, lflw, lfl, lflh, fc, bi;

	passed_tests = 0;
	dig = uns = str = cha = oct = hexa = hexahard = mem = wid = crash = mod = pre = flo = lflw = lfl = lflh = fc = bi = 0;
	if (ac < 2)
		return (print_usage());
	else if (atoi(av[1]) == 0 && is_num(av[1]))
	{
		dig = test_digits();
		fflush(stdout);
		uns = test_unsigned();
		fflush(stdout);
		str = test_strings();
		fflush(stdout);
		cha = test_chars();
		fflush(stdout);
		oct = test_octal();
		fflush(stdout);
		hexa = test_hexadecimal();
		fflush(stdout);
		hexahard = test_hexadecimal_hard();
		fflush(stdout);
		mem = test_memory();
		fflush(stdout);
		wid = test_width_parameters();
		fflush(stdout);
		crash = test_crashtest();
		fflush(stdout);
		mod = test_modifiers();
		fflush(stdout);
		pre = test_precision();
		fflush(stdout);
		flo = test_float();
		fflush(stdout);
		lflw = test_float_long_wrong();
		fflush(stdout);
		lfl = test_float_long();
		fflush(stdout);
		lflh = test_float_hard();
		fflush(stdout);
		fc = test_filechecker();
		fflush(stdout);
		bi = test_binary();
		fflush(stdout);
		passed_tests = dig + uns + str + cha + oct + hexa + hexahard + mem + wid + crash + mod + pre + flo + lflw + lfl + lflh + fc + bi;
		if (dig == 62)
			printf("\n\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", " 1 - DIGITS:", dig, 62);
		else
			printf("\n\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", " 1 - DIGITS:", dig, 62);
		if (uns == 14)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", " 2 - UNSIGNED:", uns, 14);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", " 2 - UNSIGNED:", uns, 14);
		if (str == 21)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", " 3 - STRINGS:", str, 21);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "  - STRINGS:", str, 21);
		if (cha == 20)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", " 4 - CHARS:", cha, 20);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", " 4 - CHARS:", cha, 20);
		if (oct == 33)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", " 5 - OCTAL:", oct, 33);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", " 5 - OCTAL:", oct, 33);
		if (hexa == 9)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", " 6 - HEXADECIMAL:", hexa, 9);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", " 6 - HEXADECIMAL:", hexa, 9);
		if (hexahard == 19)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", " 7 - HEXADECIMAL HARD:", hexahard, 19);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", " 7 - HEXADECIMAL HARD:", hexahard, 19);
		if (mem == 51)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", " 8 - MEMORY:", mem, 51);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", " 8 - MEMORY:", mem, 51);
		if (wid == 12)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", " 9 - WIDTH PARAMETERS:", wid, 12);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", " 9 - WIDTH PARAMETERS:", wid, 12);
		if (crash == 11)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", "10 - CRASHTEST:", crash, 11);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "10 - CRASHTEST:", crash, 11);
		if (mod == 54)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", "11 - MODIFIERS:", mod, 54);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "11 - MODIFIERS:", mod, 54);
		if (pre == 30)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", "12 - PRECISION:", pre, 30);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "12 - PRECISION:", pre, 30);
		if (flo == 66)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", "13 - FLOAT:", flo, 66);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "13 - FLOAT:", flo, 66);
		if (lflw == 71)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", "14 - FLOAT LONG WRONG:", lflw, 71);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "14 - FLOAT LONG WRONG:", lflw, 71);
		if (lfl == 71)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", "15 - FLOAT LONG:", lfl, 71);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "15 - FLOAT LONG:", lfl, 71);
		if (lflh == 2)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", "16 - FLOAT HARD:", lflh, 2);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "16 - FLOAT HARD:", lflh, 2);
		if (fc == 206)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", "17 - FILECHECKER:", fc, 206);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "17 - FILECHECKER:", fc, 206);
		if (bi == 1)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", "18 - BINARIES:", bi, 1);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "18 - BINARIES:", bi, 1);
		if (passed_tests == 753)
			printf("\033[1;32m%-18s\t%3d / %3d tests passed\033[0m\n", "FINAL:", passed_tests, 753);
		else
			printf("\033[1;31m%-18s\t%3d / %3d tests passed\033[0m\n", "FINAL:", passed_tests, 753);
	}
	else if (atoi(av[1]) == 1 && is_num(av[1]))
		test_digits();
	else if (atoi(av[1]) == 2 && is_num(av[1]))
		test_unsigned();
	else if (atoi(av[1]) == 3 && is_num(av[1]))
		test_strings();
	else if (atoi(av[1]) == 4 && is_num(av[1]))
		test_chars();
	else if (atoi(av[1]) == 5 && is_num(av[1]))
		test_octal();
	else if (atoi(av[1]) == 6 && is_num(av[1]))
		test_hexadecimal();
	else if (atoi(av[1]) == 7 && is_num(av[1]))
		test_hexadecimal_hard();
	else if (atoi(av[1]) == 8 && is_num(av[1]))
		test_memory();
	else if (atoi(av[1]) == 9 && is_num(av[1]))
		test_width_parameters();
	else if (atoi(av[1]) == 10 && is_num(av[1]))
		test_crashtest();
	else if (atoi(av[1]) == 11 && is_num(av[1]))
		test_modifiers();
	else if (atoi(av[1]) == 12 && is_num(av[1]))
		test_precision();
	else if (atoi(av[1]) == 13 && is_num(av[1]))
		test_float();
	else if (atoi(av[1]) == 14 && is_num(av[1]))
		test_float_long_wrong();
	else if (atoi(av[1]) == 15 && is_num(av[1]))
		test_float_long();
	else if (atoi(av[1]) == 16 && is_num(av[1]))
		test_float_hard();
	else if (atoi(av[1]) == 17 && is_num(av[1]))
		test_filechecker();
	else if (atoi(av[1]) == 18 && is_num(av[1]))
		test_binary();
	// else
		// return (print_usage());
	return (0);
}
