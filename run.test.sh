#!/bin/bash
GCC="gcc"
ROOT_PATH="/Users/edjubert/Documents/rendu/ft_printf/"

while getopts avhr option
do
case "${option}"
in
a) ALL=TRUE;;
h) HELP=TRUE;;
v) VERBOSE=TRUE;;
b) BUILD=TRUE;;
r) REBUILD=TRUE;;
esac
done

shift $((OPTIND-1))
print_usage() {
	echo "Usage:\ttest\t[-avhrR]\ttest_nb\tnb_of_run\n"
	echo
	echo "\toptions:"
	echo "\t\t-a: run all tests"
	echo "\t\t-v: verbose mode"
	echo "\t\t-h: print usage"
	echo "\t\t-r: rebuild printf"
	echo "\t\t-b: rebuild all"
	echo
	echo "\ttests index:"
	echo "\t\t0: run all tests"
	echo "\t\t1: test digits"
	echo "\t\t2: test unsigned"
	echo "\t\t3: test strings"
	echo "\t\t4: test chars"
	echo "\t\t5: test octal"
	echo "\t\t6: test hexadecimal"
	echo "\t\t7: test hexadecimal HARD"
	echo "\t\t8: test memory"
	echo "\t\t9: test width parameters"
	echo "\t\t10: test crashtest"
	echo "\t\t11: test modifiers"
	echo "\t\t12: test precision"
	echo "\t\t13: test float"
	echo "\t\t14: test float long wrong"
	echo "\t\t15: test float long"
	echo "\t\t16: test float hard"
	echo "\t\t17: test filechecker"
	echo "\t\t18: test binary"
}

case $REBUILD in
	TRUE)  make -C "$ROOT_PATH" rebuild; echo "rebuild done...";;
esac

case $BUILD in
	TRUE)   make -C "$ROOT_PATH" re; echo "rebuild done...";;
esac

${GCC}	$(PWD)/main.c						\
		$(PWD)/test.digits.c				\
		$(PWD)/test.unsigned.c				\
		$(PWD)/test.strings.c				\
		$(PWD)/test.chars.c					\
		$(PWD)/test.octal.c					\
		$(PWD)/test.hexadecimal.c			\
		$(PWD)/test.hexadecimal.hard.c		\
		$(PWD)/test.memory.c				\
		$(PWD)/test.width.parameters.c		\
		$(PWD)/test.crashtest.c				\
		$(PWD)/test.modifiers.c				\
		$(PWD)/test.precision.c				\
		$(PWD)/test.float.c					\
		$(PWD)/test.float.long.wrong.c		\
		$(PWD)/test.float.long.c			\
		$(PWD)/test.float.hard.c			\
		$(PWD)/test.filechecker.c			\
		$(PWD)/test.binary.c				\
		$(PWD)/test.tool.c					\
		"$ROOT_PATH"/libftprintf.a			\
		"$ROOT_PATH"/libft/libft.a			\
		-I "$ROOT_PATH"tests.h				\
		-o "$ROOT_PATH"tests 2> error.logs

if [ $? -ne 0 ]
then
	echo "Compile failed"
	cat $(PWD)/error.logs | grep -A 2 "error"
	cat $(PWD)/error.logs | grep -A 99 "Undefined symbols for "
	exit 1
fi

case $HELP in
	TRUE) print_usage;;
esac

ARG=$1
case $ALL in
	TRUE)   ARG=0;;
esac

counter=0

if [ ! -z "$2" ]
	then
		echo "\n\nRunning command \"./tests $1\" $2 time"
		while [ $counter -lt $2 ]
		do
			$(PWD)/tests $1
			if [ $? -ne 0 ]
			then
				i=0
				while [ $i -lt 100 ]
				do
					
					echo "\033c"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\033[91m                    |**********************************************************|\033[0m"
					echo "\033[91m                    |                                                          |\033[0m"
					echo "\033[91m                    |                     /!\  \033[33mGAME \033[31mOVER\033[91m  /!\                  |\033[0m"
					echo "\033[91m                    |                                                          |\033[0m"
					echo "\033[91m                    |**********************************************************|\033[0m"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n"
					sleep 0.1
					echo "\033c"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\033[91m                    /**********************************************************\\ \033[0m"
					echo "\033[91m                    /                                                          \\ \033[0m"
					echo "\033[91m                    /                     /!\  \033[31mGAME \033[33mOVER\033[91m  /!\                  \\ \033[0m"
					echo "\033[91m                    /                                                          \\ \033[0m"
					echo "\033[91m                    /**********************************************************\\ \033[0m"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n"
					sleep 0.1
					echo "\033c"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\033[91m                    -**********************************************************-\033[0m"
					echo "\033[91m                    -                                                          -\033[0m"
					echo "\033[91m                    -                          \033[33mGAME \033[31mOVER\033[91m                       -\033[0m"
					echo "\033[91m                    -                                                          -\033[0m"
					echo "\033[91m                    -**********************************************************-\033[0m"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n"
					sleep 0.1
					echo "\033c"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\033[91m                    \\**********************************************************/\033[0m"
					echo "\033[91m                    \\                                                          /\033[0m"
					echo "\033[91m                    \\                          \033[31mGAME \033[33mOVER\033[91m                       /\033[0m"
					echo "\033[91m                    \\                                                          /\033[0m"
					echo "\033[91m                    \\**********************************************************/\033[0m"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n\n\n\n\n\n\n\n\n\n"
					echo "\n\n"
					sleep 0.1
					((i++))
				done
				exit
			fi
			((counter++))
		done
			echo "\n\n"
			echo "\033[92m                    |**********************************************************|\033[0m"
			echo "\033[92m                    |                                                          |\033[0m"
			echo "\033[92m                    |                      END WITH SUCCESS                    |\033[0m"
			echo "\033[92m                    |                                                          |\033[0m"
			echo "\033[92m                    |**********************************************************|\033[0m"	
			echo "\n\n"
elif [ ! -z "$1" ]
	then
		echo "\n\nRunning command \""$(PWD)"/tests $1\""
		$(PWD)/tests $1
		$(PWD)/tests $1 | grep "MINE " > $(PWD)/test.output
else
	print_usage
fi

case $VERBOSE in
	TRUE)	echo;echo "<== error.logs ==>";cat $(PWD)/error.logs;echo;echo "<==  test.output ==>";cat $(PWD)/test.output;;
esac
