/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.hexadecimal.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 14:46:32 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/28 16:03:51 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_hexadecimal(void)
{
	int
	mr, or, all, num;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;
	printf("\033[1;37m======== %%x and %%X ========\033[0m\n");

	mr = ft_printf("MINE>\t[%x]\n", 16);
	or = printf("ORIG>\t[%x]\n", 16);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%x]\n", 2048);
	or = printf("ORIG>\t[%x]\n", 2048);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%x]\n", 16777215);
	or = printf("ORIG>\t[%x]\n", 16777215);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%X]\n", 6777215);
	or = printf("ORIG>\t[%X]\n", 6777215);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%#x]\n", 1677215);
	or = printf("ORIG>\t[%#x]\n", 1677215);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%#X]\n", 1677725);
	or = printf("ORIG>\t[%#X]\n", 1677725);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%#+x]\n", 1777215);
	or = printf("ORIG>\t[%#+x]\n", 1777215);
	num++;
	all += print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%#-X]\n", 16215);
	or = printf("ORIG>\t[%#-X]\n", 16215);
	num++;
	all +=print_result(mr, or, __LINE__);

	mr = ft_printf("MINE>\t[%+20.10x]\n", -777);
	or = printf("ORIG>\t[%+20.10x]\n", -777);
	num++;
	all +=print_result(mr, or, __LINE__);

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\n\033[1;37m======= end %%x and %%X ========\033[0m\n\n");
	return (all);
}
