/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.strings.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 11:55:22 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/26 14:31:42 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int test_strings(void)
{
	int
		mr,
		or, all, num;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;
	printf("\033[1;37m======== %%s ========\033[0m\n");

	printf("\nTEST: %%s -> \"\"\n");
	mr = ft_printf("MINE>\t[%s]\n", "");
	or = printf("ORIG>\t[%s]\n", "");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%10s -> \"\"\n");
	mr = ft_printf("MINE>\t[%10s]\n", "");
	or = printf("ORIG>\t[%10s]\n", "");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%s -> NULL\n");
	mr = ft_printf("MINE>\t[%s]\n", NULL);
	or = printf("ORIG>\t[%s]\n", NULL);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%s]\n", "Coconut");
	or = printf("ORIG>\t[%s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[% s]\n", "Coconut");
	or = printf("ORIG>\t[% s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% 2s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[% 2s]\n", "Coconut");
	or = printf("ORIG>\t[% 2s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-4s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%-4s]\n", "Coconut");
	or = printf("ORIG>\t[%-4s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+8s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%+8s]\n", "Coconut");
	or = printf("ORIG>\t[%+8s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+15s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%+15s]\n", "Coconut");
	or = printf("ORIG>\t[%+15s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-15s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%-15s]\n", "Coconut");
	or = printf("ORIG>\t[%-15s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%15s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%15s]\n", "Coconut");
	or = printf("ORIG>\t[%15s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-50.20s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%-50.20s]\n", "Coconut");
	or = printf("ORIG>\t[%-50.20s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%50.20s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%50.20s]\n", "Coconut");
	or = printf("ORIG>\t[%50.20s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%020s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%020s]\n", "Coconut");
	or = printf("ORIG>\t[%020s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+020s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%+020s]\n", "Coconut");
	or = printf("ORIG>\t[%+020s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-020s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%+-020s]\n", "Coconut");
	or = printf("ORIG>\t[%+-020s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-+020s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%-+020s]\n", "Coconut");
	or = printf("ORIG>\t[%-+020s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%05s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%05s]\n", "Coconut");
	or = printf("ORIG>\t[%05s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+50.25s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%+50.25s]\n", "Coconut");
	or = printf("ORIG>\t[%+50.25s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#50.25s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[%#50.25s]\n", "Coconut");
	or = printf("ORIG>\t[%#50.25s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% 50.25s -> \"Coconut\"\n");
	mr = ft_printf("MINE>\t[% 50.25s]\n", "Coconut");
	or = printf("ORIG>\t[% 50.25s]\n", "Coconut");
	all += print_result(mr, or, __LINE__);
	num++;

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end %%s =====\033[0m\n\n");
	return (all);
}
