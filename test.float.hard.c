/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.float.hard.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 10:07:37 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/01 17:28:06 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_float_hard(void)
{
	int
	mr, or, all, num;

	mr = or = all = num = 0;
	printf("\033[1;37m======== %%f HARD ========\033[0m\n");
	
	printf("\nTEST>\t[%%Lf] -> 9223372036854775807.0L\n");
	mr = ft_printf("MINE>\t[%Lf]\n", 9223372036854775807.0L);
	fflush(stdout);
	or = printf("ORIG>\t[%Lf]\n", 9223372036854775807.0L);
	all += print_result(mr, or, __LINE__);
	num++;
	
	printf("\nTEST>\t[%%Lf] -> 18446744073709551615.0L\n");
	mr = ft_printf("MINE>\t[%Lf]\n", 18446744073709551615.0L);
	fflush(stdout);
	or = printf("ORIG>\t[%Lf]\n", 18446744073709551615.0L);
	all += print_result(mr, or, __LINE__);
	num++;

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end %%f HARD =====\033[0m\n\n");
	return (all);
}
