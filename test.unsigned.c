/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.unsigned.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 11:39:02 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/19 19:39:20 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int test_unsigned(void)
{
	int
		mr,
		or, all, num;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;
	printf("\033[1;37m======== %%u ========\033[0m\n");

	printf("\nTEST: %%u -> 650\n");
	mr = ft_printf("MINE>\t[%u]\n", 650);
	or = printf("ORIG>\t[%u]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%u -> 4000000000\n");
	mr = ft_printf("MINE>\t[%u]\n", 4000000000);
	or = printf("ORIG>\t[%u]\n", 4000000000);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%u -> -650\n");
	mr = ft_printf("MINE>\t[%u]\n", -650);
	or = printf("ORIG>\t[%u]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-10u -> 12345\n");
	mr = ft_printf("MINE>\t[%+-10u]\n", 12345);
	or = printf("ORIG>\t[%+-10u]\n", 12345);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+10u -> 12345\n");
	mr = ft_printf("MINE>\t[%+10u]\n", 12345);
	or = printf("ORIG>\t[%+10u]\n", 12345);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%20.10u -> 650\n");
	mr = ft_printf("MINE>\t[%20.10u]\n", 650);
	or = printf("ORIG>\t[%20.10u]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%20.10u -> 4000000000\n");
	mr = ft_printf("MINE>\t[%20.10u]\n", 4000000000);
	or = printf("ORIG>\t[%20.10u]\n", 4000000000);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%20.10u -> -650\n");
	mr = ft_printf("MINE>\t[%20.10u]\n", -650);
	or = printf("ORIG>\t[%20.10u]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#20.10u -> 650\n");
	mr = ft_printf("MINE>\t[%#20.10u]\n", 650);
	or = printf("ORIG>\t[%#20.10u]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#20.10u -> 4000000000\n");
	mr = ft_printf("MINE>\t[%#20.10u]\n", 4000000000);
	or = printf("ORIG>\t[%#20.10u]\n", 4000000000);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#20.10u -> -650\n");
	mr = ft_printf("MINE>\t[%#20.10u]\n", -650);
	or = printf("ORIG>\t[%#20.10u]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%20.10lu -> 650\n");
	mr = ft_printf("MINE>\t[%20.10lu]\n", 650);
	or = printf("ORIG>\t[%20.10lu]\n", 650);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%20.10lu -> 4000000000\n");
	mr = ft_printf("MINE>\t[%20.10lu]\n", 4000000000);
	or = printf("ORIG>\t[%20.10lu]\n", 4000000000);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%20.10lu -> -650\n");
	mr = ft_printf("MINE>\t[%20.10lu]\n", -650);
	or = printf("ORIG>\t[%20.10lu]\n", -650);
	all += print_result(mr, or, __LINE__);
	num++;

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end %%u =====\033[0m\n\n");
	return (all);
}
