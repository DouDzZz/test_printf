/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.float.long.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 20:03:49 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/01 17:57:16 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_float_long(void)
{
	int
	mr, or, all, num;

	mr = or = all = num = 0;
	printf("\033[1;37m======== %%Lf ========\033[0m\n");

	fflush(stdout);
	printf("\nTEST>\t[%%Lf] -> 0.0\n");
	mr = ft_printf("MINE>\t[%Lf]\n", 0.0L);
	fflush(stdout);
	or = printf("ORIG>\t[%Lf]\n", 0.0L);
	all += print_result(mr, or, __LINE__);
	num++;
	
	printf("\nTEST>\t[%%.3Lf] -> 0.9995\n");
	mr = ft_printf("MINE>\t[%.3Lf]\n", 0.9995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.3Lf]\n", 0.9995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.4Lf] -> 0.9995\n");
	mr = ft_printf("MINE>\t[%.4Lf]\n", 0.9995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.4Lf]\n", 0.9995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.3Lf] -> 0.5555\n");
	mr = ft_printf("MINE>\t[%.3Lf]\n", 0.5555L);
	fflush(stdout);
	or = printf("ORIG>\t[%.3Lf]\n", 0.5555L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.3Lf] -> 0.59995\n");
	mr = ft_printf("MINE>\t[%.3Lf]\n", 0.59995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.3Lf]\n", 0.59995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.4Lf] -> 0.59995\n");
	mr = ft_printf("MINE>\t[%.4Lf]\n", 0.59995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.4Lf]\n", 0.59995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.5Lf] -> 0.59995\n");
	mr = ft_printf("MINE>\t[%.5Lf]\n", 0.59995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.5Lf]\n", 0.59995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.6Lf] -> 0.59995\n");
	mr = ft_printf("MINE>\t[%.6Lf]\n", 0.59995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.6Lf]\n", 0.59995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.2Lf] -> 0.59995\n");
	mr = ft_printf("MINE>\t[%.2Lf]\n", 0.59995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.2Lf]\n", 0.59995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.2Lf] -> 0.559995\n");
	mr = ft_printf("MINE>\t[%.2Lf]\n", 0.559995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.2Lf]\n", 0.559995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.3Lf] -> 0.5559995\n");
	mr = ft_printf("MINE>\t[%.3Lf]\n", 0.5559995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.3Lf]\n", 0.5559995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.4Lf] -> 0.5559995\n");
	mr = ft_printf("MINE>\t[%.4Lf]\n", 0.5559995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.4Lf]\n", 0.5559995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.5Lf] -> 0.5559995\n");
	mr = ft_printf("MINE>\t[%.5Lf]\n", 0.5559995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.5Lf]\n", 0.5559995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.6Lf] -> 0.5559995\n");
	mr = ft_printf("MINE>\t[%.6Lf]\n", 0.5559995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.6Lf]\n", 0.5559995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.7Lf] -> 0.5559995\n");
	mr = ft_printf("MINE>\t[%.7Lf]\n", 0.5559995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.7Lf]\n", 0.5559995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%Lf] -> 127.123456789123456789123456789123456789\n");
	mr = ft_printf("MINE>\t[%Lf]\n", 127.123456789123456789123456789123456789L);
	fflush(stdout);
	or = printf("ORIG>\t[%Lf]\n", 127.123456789123456789123456789123456789L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%Lf] -> 127.123456\n");
	mr = ft_printf("MINE>\t[%Lf]\n", 127.123456L);
	fflush(stdout);
	or = printf("ORIG>\t[%Lf]\n", 127.123456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.Lf] -> 127.123456\n");
	mr = ft_printf("MINE>\t[%.Lf]\n", 127.123456L);
	fflush(stdout);
	or = printf("ORIG>\t[%.Lf]\n", 127.123456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%.Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%.Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%20.Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%20.Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%20.Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%20.Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%20.Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%20.Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#20.Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%#20.Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%#20.Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#20Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%#20Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%#20Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%-20.Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%-20.Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%-20.Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%-20.Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%-20.Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%-20.Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#-20.Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%#-20.Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%#-20.Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#-20Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%#-20Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%#-20Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%% .Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[% .Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[% .Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%# .Lf] -> 127.923456\n");
	mr = ft_printf("MINE>\t[%# .Lf]\n", 127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%# .Lf]\n", 127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% f -> 127.9996\n");
	mr = ft_printf("MINE>\t[% Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[% Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%#Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%#Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%# f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%# Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%# Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%-Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%-Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%+Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%+Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%+-Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%+-Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%0f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%0Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%0Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%0.15f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%0.14Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%0.14Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#0.15f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%#0.14Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%#0.14Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#+0.15f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%#+0.14Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%#+0.14Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%# +0.15f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%# +0.14Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%# +0.14Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%# +-0.15f -> 127.9996\n");
	mr = ft_printf("MINE>\t[%# +-0.14Lf]\n",  127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%# +-0.14Lf]\n", 127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("TEST>\t[%%.3Lf] -> -0.4995\n");
	mr = ft_printf("MINE>\t[%.3Lf]\n", -0.4995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.3Lf]\n", -0.4995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.3Lf] -> -0.0\n");
	mr = ft_printf("MINE>\t[%.3Lf]\n", -0.0L);
	fflush(stdout);
	or = printf("ORIG>\t[%.3Lf]\n", -0.0L);
	all += print_result(mr, or, __LINE__);
	num++;
  
	printf("\nTEST>\t[%%.3Lf] -> 0.0\n");
	mr = ft_printf("MINE>\t[%.3Lf]\n", 0.0L);
	fflush(stdout);
	or = printf("ORIG>\t[%.3Lf]\n", 0.0L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.3Lf] -> -0.9995\n");
	mr = ft_printf("MINE>\t[%.3Lf]\n", -0.9995L);
	fflush(stdout);
	or = printf("ORIG>\t[%.3Lf]\n", -0.9995L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%Lf] -> -127.123456789123456789123456789123456789\n");
	mr = ft_printf("MINE>\t[%Lf]\n", -127.123456789123456789123456789123456789L);
	fflush(stdout);
	or = printf("ORIG>\t[%Lf]\n", -127.123456789123456789123456789123456789L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%Lf] -> -127.123456\n");
	mr = ft_printf("MINE>\t[%Lf]\n", -127.123456L);
	fflush(stdout);
	or = printf("ORIG>\t[%Lf]\n", -127.123456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.Lf] -> -127.123456\n");
	mr = ft_printf("MINE>\t[%.Lf]\n", -127.123456L);
	fflush(stdout);
	or = printf("ORIG>\t[%.Lf]\n", -127.123456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%.Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%.Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%.Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%20.Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%20.Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%20.Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%20.Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%20.Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%20.Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#20.Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%#20.Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%#20.Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#20Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%#20Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%#20Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%-20.Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%-20.Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%-20.Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%-20.Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%-20.Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%-20.Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#-20.Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%#-20.Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%#-20.Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%#-20Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%#-20Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%#-20Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%% .Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[% .Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[% .Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST>\t[%%# .Lf] -> -127.923456\n");
	mr = ft_printf("MINE>\t[%# .Lf]\n", -127.923456L);
	fflush(stdout);
	or = printf("ORIG>\t[%# .Lf]\n", -127.923456L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[% Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[% Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%#Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%#Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%# Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%# Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%# Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%-Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%-Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%+Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%+Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%+-Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%+-Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%0Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%0Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%0Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%0.14Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%0.14Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%0.14Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#0.14Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%#0.14Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%#0.14Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#+0.14Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%#+0.14Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%#+0.14Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%# +0.14Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%# +0.14Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%# +0.14Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%# +-0.14Lf -> -127.9996\n");
	mr = ft_printf("MINE>\t[%# +-0.14Lf]\n",  -127.9996L);
	fflush(stdout);
	or = printf("ORIG>\t[%# +-0.14Lf]\n", -127.9996L);
	all += print_result(mr, or, __LINE__);
	num++;

	// printf("\nTEST: %%.15Lf -> -127.9996L\n");
	// mr = ft_printf("MINE>\t[%.15Lf]\n",  -127.9996L);
	// fflush(stdout);
	// or = printf("ORIG>\t[%.15Lf]\n", -127.9996L);
	// all += print_result(mr, or, __LINE__);
	// num++;

	// printf("\nTEST: %%.16Lf -> -127.9996L\n");
	// mr = ft_printf("MINE>\t[%.16Lf]\n",  -127.9996L);
	// fflush(stdout);
	// or = printf("ORIG>\t[%.16Lf]\n", -127.9996L);
	// all += print_result(mr, or, __LINE__);
	// num++;

	// printf("\nTEST: %%.17Lf -> -127.9996L\n");
	// mr = ft_printf("MINE>\t[%.17Lf]\n",  -127.9996L);
	// fflush(stdout);
	// or = printf("ORIG>\t[%.17Lf]\n", -127.9996L);
	// all += print_result(mr, or, __LINE__);
	// num++;

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end %%Lf =====\033[0m\n\n");
	return (all);
}
