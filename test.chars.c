/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.chars.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 14:38:48 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/27 15:49:52 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_chars(void)
{
	int
	mr, or, all, num;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;
	printf("\033[1;37m======== %%c ========\033[0m\n");

	mr = ft_printf("MINE>\t[%c]\n", '"');
	or = printf("ORIG>\t[%c]\n", '"');
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("MINE>\t[%+10c]\n", 'r');
	or = printf("ORIG>\t[%+10c]\n", 'r');
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("MINE>\t[%-10c]\n", 'r');
	or = printf("ORIG>\t[%-10c]\n", 'r');
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("MINE>\t[%010c]\n", 'r');
	or = printf("ORIG>\t[%010c]\n", 'r');
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("MINE>\t[%+-10c]\n", 'r');
	or = printf("ORIG>\t[%+-10c]\n", 'r');
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("MINE>\t[%0+10c]\n", 'r');
	or = printf("ORIG>\t[%0+10c]\n", 'r');
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("MINE>\t[%05c]\n", 'r');
	or = printf("ORIG>\t[%05c]\n", 'r');
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("MINE>\t[%10.5c]\n", 'r');
	or = printf("ORIG>\t[%10.5c]\n", 'r');
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("MINE>\t[%-10.5c]\n", 'r');
	or = printf("ORIG>\t[%-10.5c]\n", 'r');
	all += print_result(mr, or, __LINE__);
	num++;

	mr = ft_printf("MINE>\t[%5c]\n", 'r');
	or = printf("ORIG>\t[%5c]\n", 'r');
	all += print_result(mr, or, __LINE__);
	num++;


	printf("\nTEST: %%-50.20c -> \'c\'\n");
	mr = ft_printf("MINE>\t[%-50.20c]\n", 'c');
	or = printf("ORIG>\t[%-50.20c]\n", 'c');
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%50.20c -> \'c\'\n");
	mr = ft_printf("MINE>\t[%50.20c]\n", 'c');
	or = printf("ORIG>\t[%50.20c]\n", 'c');
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%020c -> \'c\'\n");
	mr = ft_printf("MINE>\t[%020c]\n", 'c');
	or = printf("ORIG>\t[%020c]\n", 'c');
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+020c -> \'c\'\n");
	mr = ft_printf("MINE>\t[%+020c]\n", 'c');
	or = printf("ORIG>\t[%+020c]\n", 'c');
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+-020c -> \'c\'\n");
	mr = ft_printf("MINE>\t[%+-020c]\n", 'c');
	or = printf("ORIG>\t[%+-020c]\n", 'c');
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%-+020c -> \'c\'\n");
	mr = ft_printf("MINE>\t[%-+020c]\n", 'c');
	or = printf("ORIG>\t[%-+020c]\n", 'c');
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%05c -> \'c\'\n");
	mr = ft_printf("MINE>\t[%05c]\n", 'c');
	or = printf("ORIG>\t[%05c]\n", 'c');
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%+50.25c -> \'c\'\n");
	mr = ft_printf("MINE>\t[%+50.25c]\n", 'c');
	or = printf("ORIG>\t[%+50.25c]\n", 'c');
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %%#50.25c -> \'c\'\n");
	mr = ft_printf("MINE>\t[%#50.25c]\n", 'c');
	or = printf("ORIG>\t[%#50.25c]\n", 'c');
	all += print_result(mr, or, __LINE__);
	num++;

	printf("\nTEST: %% 50.25c -> \'c\'\n");
	mr = ft_printf("MINE>\t[% 50.25c]\n", 'c');
	or = printf("ORIG>\t[% 50.25c]\n", 'c');
	all += print_result(mr, or, __LINE__);
	num++;

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end %%c =====\033[0m\n\n");
	return (all);
}
