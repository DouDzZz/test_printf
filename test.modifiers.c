/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.modifiers.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 15:31:54 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/28 16:38:15 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_modifiers(void)
{
	int
	mr, or, all, num;
	size_t
	st = -256;
    ptrdiff_t
	diff = &mr - &or;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;

	printf("\033[1;37m==== %%d + modifiers ===\033[0m\n");

	ft_printf("{red}%T{eoc}", "D MODIFIER");

	printf("TEST>\t[%%hd] ->  30000\n");
	mr = ft_printf("MINE>\t[%hd]\n", 30000);
	or = printf(   "ORIG>\t[%hd]\n", 30000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hd] ->  3000\n");
	mr = ft_printf("MINE>\t[%hd]\n", 3000);
	or = printf(   "ORIG>\t[%hd]\n", 3000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hd] ->  300000\n");
	mr = ft_printf("MINE>\t[%hd]\n", 300000);
	or = printf(   "ORIG>\t[%hd]\n", 300000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hd] ->  -150\n");
	mr = ft_printf("MINE>\t[%hd]\n", -150);
	or = printf(   "ORIG>\t[%hd]\n", -150);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hd] ->  15\n");
	mr = ft_printf("MINE>\t[%hd]\n", 15);
	or = printf(   "ORIG>\t[%hd]\n", 15);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hhd] ->  300\n");
	mr = ft_printf("MINE>\t[%hhd]\n", 300);
	or = printf(   "ORIG>\t[%hhd]\n", 300);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hhd] ->  -150\n");
	mr = ft_printf("MINE>\t[%hhd]\n", -150);
	or = printf(   "ORIG>\t[%hhd]\n", -150);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hhd] ->  15\n");
	mr = ft_printf("MINE>\t[%hhd]\n", 15);
	or = printf(   "ORIG>\t[%hhd]\n", 15);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hhd] ->  123456789\n");
	mr = ft_printf("MINE>\t[%hhd]\n", 123456789);
	or = printf(   "ORIG>\t[%hhd]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hd] ->  123456789\n");
	mr = ft_printf("MINE>\t[%hd]\n", 123456789);
	or = printf(   "ORIG>\t[%hd]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%20.10hhd] ->  123456789\n");
	mr = ft_printf("MINE>\t[%20.10hhd]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10hhd]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("test 20.10hd\n");
	mr = ft_printf("MINE>\t[%20.10hd]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10hd]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%lld] ->  123456789\n");
	mr = ft_printf("MINE>\t[%lld]\n", 123456789);
	or = printf(   "ORIG>\t[%lld]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%ld] ->  123456789\n");
	mr = ft_printf("MINE>\t[%ld]\n", 123456789);
	or = printf(   "ORIG>\t[%ld]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);
	
	printf("TEST>\t[%%20.10lld] ->  123456789\n");
	mr = ft_printf("MINE>\t[%20.10lld]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10lld]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%20.10ld] ->  123456789\n");
	mr = ft_printf("MINE>\t[%20.10ld]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10ld]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);
	
	printf("TEST>\t[%%20.10lld] ->  -> 123456789\n");
	mr = ft_printf("MINE>\t[%20.10lld]\n", -123456789);
	or = printf(   "ORIG>\t[%20.10lld]\n", -123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%20.10ld] ->  -> 123456789\n");
	mr = ft_printf("MINE>\t[%20.10ld]\n", -123456789);
	or = printf(   "ORIG>\t[%20.10ld]\n", -123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("{red}%T{eoc}", "X MODIFIER");

	printf("TEST>\t[%%hx] ->  30000\n");
	mr = ft_printf("MINE>\t[%hx]\n", 30000);
	or = printf(   "ORIG>\t[%hx]\n", 30000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hx] ->  3000\n");
	mr = ft_printf("MINE>\t[%hx]\n", 3000);
	or = printf(   "ORIG>\t[%hx]\n", 3000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hx] ->  300000\n");
	mr = ft_printf("MINE>\t[%hx]\n", 300000);
	or = printf(   "ORIG>\t[%hx]\n", 300000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hx] ->  -150\n");
	mr = ft_printf("MINE>\t[%hx]\n", -150);
	or = printf(   "ORIG>\t[%hx]\n", -150);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hx] ->  15\n");
	mr = ft_printf("MINE>\t[%hx]\n", 15);
	or = printf(   "ORIG>\t[%hx]\n", 15);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hhx] ->  300\n");
	mr = ft_printf("MINE>\t[%hhx]\n", 300);
	or = printf(   "ORIG>\t[%hhx]\n", 300);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hhx] ->  -150\n");
	mr = ft_printf("MINE>\t[%hhx]\n", -150);
	or = printf(   "ORIG>\t[%hhx]\n", -150);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hhx] ->  15\n");
	mr = ft_printf("MINE>\t[%hhx]\n", 15);
	or = printf(   "ORIG>\t[%hhx]\n", 15);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hhx] ->  123456789\n");
	mr = ft_printf("MINE>\t[%hhx]\n", 123456789);
	or = printf(   "ORIG>\t[%hhx]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hx] ->  123456789\n");
	mr = ft_printf("MINE>\t[%hx]\n", 123456789);
	or = printf(   "ORIG>\t[%hx]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%20.10hhx] ->  123456789\n");
	mr = ft_printf("MINE>\t[%20.10hhx]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10hhx]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("test 20.10hd\n");
	mr = ft_printf("MINE>\t[%20.10hx]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10hx]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%llx] ->  123456789\n");
	mr = ft_printf("MINE>\t[%llx]\n", 123456789);
	or = printf(   "ORIG>\t[%llx]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%lx] ->  123456789\n");
	mr = ft_printf("MINE>\t[%lx]\n", 123456789);
	or = printf(   "ORIG>\t[%lx]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);
	
	printf("TEST>\t[%%20.10llx] ->  123456789\n");
	mr = ft_printf("MINE>\t[%20.10llx]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10llx]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%20.10lx] ->  123456789\n");
	mr = ft_printf("MINE>\t[%20.10lx]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10lx]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);
	
	printf("TEST>\t[%%20.10llx] ->  -> 123456789\n");
	mr = ft_printf("MINE>\t[%20.10llx]\n", -123456789);
	or = printf(   "ORIG>\t[%20.10llx]\n", -123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%20.10lx] ->  -> 123456789\n");
	mr = ft_printf("MINE>\t[%20.10lx]\n", -123456789);
	or = printf(   "ORIG>\t[%20.10lx]\n", -123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("{red}%T{eoc}", "O MODIFIER");

	printf("TEST>\t[%%ho] ->  30000\n");
	mr = ft_printf("MINE>\t[%ho]\n", 30000);
	or = printf(   "ORIG>\t[%ho]\n", 30000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%ho] ->  3000\n");
	mr = ft_printf("MINE>\t[%ho]\n", 3000);
	or = printf(   "ORIG>\t[%ho]\n", 3000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%ho] ->  300000\n");
	mr = ft_printf("MINE>\t[%ho]\n", 300000);
	or = printf(   "ORIG>\t[%ho]\n", 300000);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%ho] ->  -150\n");
	mr = ft_printf("MINE>\t[%ho]\n", -150);
	or = printf(   "ORIG>\t[%ho]\n", -150);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%ho] ->  15\n");
	mr = ft_printf("MINE>\t[%ho]\n", 15);
	or = printf(   "ORIG>\t[%ho]\n", 15);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hho] ->  300\n");
	mr = ft_printf("MINE>\t[%hho]\n", 300);
	or = printf(   "ORIG>\t[%hho]\n", 300);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hho] ->  -150\n");
	mr = ft_printf("MINE>\t[%hho]\n", -150);
	or = printf(   "ORIG>\t[%hho]\n", -150);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hho] ->  15\n");
	mr = ft_printf("MINE>\t[%hho]\n", 15);
	or = printf(   "ORIG>\t[%hho]\n", 15);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%hho] ->  123456789\n");
	mr = ft_printf("MINE>\t[%hho]\n", 123456789);
	or = printf(   "ORIG>\t[%hho]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%ho] ->  123456789\n");
	mr = ft_printf("MINE>\t[%ho]\n", 123456789);
	or = printf(   "ORIG>\t[%ho]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%20.10hho] ->  123456789\n");
	mr = ft_printf("MINE>\t[%20.10hho]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10hho]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("test 20.10hd\n");
	mr = ft_printf("MINE>\t[%20.10ho]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10ho]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%llo] ->  123456789\n");
	mr = ft_printf("MINE>\t[%llo]\n", 123456789);
	or = printf(   "ORIG>\t[%llo]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%lo] ->  123456789\n");
	mr = ft_printf("MINE>\t[%lo]\n", 123456789);
	or = printf(   "ORIG>\t[%lo]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);
	
	printf("TEST>\t[%%20.10llo] ->  123456789\n");
	mr = ft_printf("MINE>\t[%20.10llo]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10llo]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%20.10lo] ->  123456789\n");
	mr = ft_printf("MINE>\t[%20.10lo]\n", 123456789);
	or = printf(   "ORIG>\t[%20.10lo]\n", 123456789);
	num++;
	all += print_result(mr, or, __LINE__);
	
	printf("TEST>\t[%%20.10llo] ->  -> 123456789\n");
	mr = ft_printf("MINE>\t[%20.10llo]\n", -123456789);
	or = printf(   "ORIG>\t[%20.10llo]\n", -123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%20.10lo] ->  -> 123456789\n");
	mr = ft_printf("MINE>\t[%20.10lo]\n", -123456789);
	or = printf(   "ORIG>\t[%20.10lo]\n", -123456789);
	num++;
	all += print_result(mr, or, __LINE__);

	// printf("TEST>\t[%%jd] ->  123456789\n");
	// mr = ft_printf("MINE>\t[%jd]\n", 123456789);
	// or = printf(   "ORIG>\t[%jd]\n", 123456789);
	// num++;
	// all += print_result(mr, or, __LINE__);

	// printf("TEST>\t[%%td] ->  123456789\n");
	// mr = ft_printf("MINE>\t[%td]\n", 123456789);
	// or = printf(   "ORIG>\t[%td]\n", 123456789);
	// num++;
	// all += print_result(mr, or, __LINE__);

	// printf("TEST>\t[%%zd] ->  123456789\n");
	// mr = ft_printf("MINE>\t[%zd]\n", 123456789);
	// or = printf(   "ORIG>\t[%zd]\n", 123456789);
	// num++;
	// all += print_result(mr, or, __LINE__);

	// printf("\nTEST: %%jd -> 65000\n");
	// mr = ft_printf("MINE>\t[%jd]\n", 65000);
	// or = printf("ORIG>\t[%jd]\n", 65000);
	// all += print_result(mr, or, __LINE__);
	// num++;

	// printf("\nTEST: %%td -> __INT32_MAX__\n");
	// mr = ft_printf("MINE>\t[%td]\n", __INT32_MAX__);
	// or = printf("ORIG>\t[%td]\n", __INT32_MAX__);
	// all += print_result(mr, or, __LINE__);
	// num++;

	// printf("\nTEST: %%zd -> __INT32_MAX__\n");
	// mr = ft_printf("MINE>\t[%zd]\n", __INT32_MAX__);
	// or = printf("ORIG>\t[%zd]\n", __INT32_MAX__);
	// all += print_result(mr, or, __LINE__);
	// num++;

	// printf("\nTEST: %%zd -> st (256)\n");
	// mr = ft_printf("MINE>\t[%zd]\n", st);
	// or = printf("ORIG>\t[%zd]\n", st);
	// all += print_result(mr, or, __LINE__);
	// num++;

    // printf(			"TEST>\t[%%td] -> diff\n");
    // mr = ft_printf(	"MINE>\t[%td]\n", diff);
    // or = printf(	"ORIG>\t[%td]\n", diff);
	// all += print_result(mr, or, __LINE__);
	// num++;
	
	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m== End %%d + modifiers ==\033[0m\n\n");
	return (all);
}
