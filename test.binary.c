/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.binary.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/01 12:39:20 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/01 12:51:21 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_binary(void)
{
	printf("\033[1;37m======== %%o ========\033[0m\n");

	printf("\nTEST>\t[%%b] -> 2048\n");
	ft_printf("MINE>\t[%b]\n", 2048);

	printf("\nTEST>\t[%% b] -> 777\n");
	ft_printf("MINE>\t[% b]\n", 777);

	printf("\nTEST>\t[%%+b] -> 128\n");
	ft_printf("MINE>\t[%+b]\n", 128);

	printf("\nTEST>\t[%%-b] -> 48\n");
	ft_printf("MINE>\t[%-b]\n", 48);

	printf("\nTEST>\t[%%+5b] -> 10\n");
	ft_printf("MINE>\t[%+5b]\n", 10);

	printf("\nTEST>\t[%%-5b] -> 2048\n");
	ft_printf("MINE>\t[%-5b]\n", 2048);

	printf("\nTEST>\t[%%+10b] -> 2048\n");
	ft_printf("MINE>\t[%+10b]\n", 2048);

	printf("\nTEST>\t[%%-+10b] -> 2048\n");
	ft_printf("MINE>\t[%-+10b]\n", 2048);

	printf("\nTEST>\t[%%-0+10x] -> 2048\n");
	ft_printf("MINE>\t[%-0+10x]\n", 2048);

	printf("\nTEST>\t[%%-0+10b] -> 2048\n");
	ft_printf("MINE>\t[%-0+10b]\n", 2048);

	printf("\nTEST>\t[%%#b] -> 48\n");
	ft_printf("MINE>\t[%#b]\n", 48);

	printf("\nTEST>\t[%%#+10b] -> 2048\n");
	ft_printf("MINE>\t[%#+10b]\n", 2048);

	printf("\nTEST>\t[%%#-0+10b] -> 2048\n");
	ft_printf("MINE>\t[%#-0+10b]\n", 2048);

	printf("\nTEST>\t[%%-0#+3b] -> 2048\n");
	ft_printf("MINE>\t[%-0#+3b]\n", 2048);

	printf("\nTEST>\t[%%b] -> 02047\n");
	ft_printf("MINE>\t[%b]\n", 02047);

	printf("\nTEST>\t[%%b] -> 02047\n");
	ft_printf("MINE>\t[%b]\n", 02047);

	printf("\nTEST>\t[%%20.10b] -> 777\n");
	ft_printf("MINE>\t[%20.10b]\n", 777);

	printf("\nTEST>\t[%%+20.10b] -> 777\n");
	ft_printf("MINE>\t[%+20.10b]\n", 777);

	printf("\nTEST>\t[%%20.10b] -> -777\n");
	ft_printf("MINE>\t[%20.10b]\n", -777);

	printf("\nTEST>\t[%%+20.10b] -> -777\n");
	ft_printf("MINE>\t[%+20.10b]\n", -777);

	printf("\nTEST>\t[%%03.2b] -> -127\n");
	ft_printf("[%03.2b]\n", -127);

	printf("\nTEST>\t[%%03.2lb] -> 0\n");
	ft_printf("[%03.2b]\n", 0);

	printf("\nTEST>\t[%%03.2lb] -> 127\n");
	ft_printf("[%03.2b]\n", 127);

	printf("\nTEST>\t[%%03.2lb] -> -127\n");
	ft_printf("[%03.2b]\n", -127);

	printf("\nTEST>\t[%%03.20lb] -> 127\n");
	ft_printf("[%03.2b]\n", 127);

	printf("\nTEST>\t[%%03.20lb] -> -127\n");
	ft_printf("[%03.2b]\n", -127);

	printf("\nTEST>\t[%%03.2llb] -> 0\n");
	ft_printf("[%03.2b]\n", 0);

	printf("\nTEST>\t[%%03.2llb] -> 127\n");
	ft_printf("[%03.2b]\n", 127);

	printf("\nTEST>\t[%%03.2llb] -> -127\n");
	ft_printf("[%03.2b]\n", -127);

	printf("\nTEST>\t[%%03.20llb] -> 127\n");
	ft_printf("[%03.2b]\n", 127);

	printf("\nTEST>\t[%%03.20llb] -> -127\n");
	ft_printf("[%03.2b]\n", -127);

	printf("\nTEST>\t[%%010b] -> 2048\n");
	ft_printf("MINE>\t[%010b]\n", 2048);

	printf("\033[1;37m======== end %%o =====\033[0m\n\n");
	return (1);
}
