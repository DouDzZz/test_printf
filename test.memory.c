/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.memory.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 14:57:16 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/20 11:48:08 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_memory(void)
{
	int
	mr, or, all, num;
	char**
	test;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;
	test = malloc(sizeof(char*));
	printf("\033[1;37m======== %%p ========\033[0m\n");

	ft_printf("TEST>\t[%%p]\n", &mr);
	mr = ft_printf("MINE>\t[%p]\n", &mr);
	or = printf(   "ORIG>\t[%p]\n", &mr);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%p]\n", &or);
	mr = ft_printf("MINE>\t[%p]\n", &or);
	or = printf(   "ORIG>\t[%p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%10p]\n", &mr);
	mr = ft_printf("MINE>\t[%10p]\n", &mr);
	or = printf(   "ORIG>\t[%10p]\n", &mr);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%+10p]\n", &or);
	mr = ft_printf("MINE>\t[%+10p]\n", &or);
	or = printf(   "ORIG>\t[%+10p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%-10p]\n", &mr);
	mr = ft_printf("MINE>\t[%-10p]\n", &mr);
	or = printf(   "ORIG>\t[%-10p]\n", &mr);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%+14p]\n", &or);
	mr = ft_printf("MINE>\t[%+14p]\n", &or);
	or = printf(   "ORIG>\t[%+14p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%+017p]\n", &or);
	mr = ft_printf("MINE>\t[%+017p]\n", &or);
	or = printf(   "ORIG>\t[%+017p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%+-017p]\n", &or);
	mr = ft_printf("MINE>\t[%+-017p]\n", &or);
	or = printf(   "ORIG>\t[%+-017p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%+#-017p]\n", &or);
	mr = ft_printf("MINE>\t[%+#-017p]\n", &or);
	or = printf(   "ORIG>\t[%+#-017p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%+-20p]\n", &or);
	mr = ft_printf("MINE>\t[%+-20p]\n", &or);
	or = printf(   "ORIG>\t[%+-20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%#+-020p]\n", &or);
	mr = ft_printf("MINE>\t[%#+-020p]\n", &or);
	or = printf(   "ORIG>\t[%#+-020p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% #+-020p]\n", &or);
	mr = ft_printf("MINE>\t[% #+-020p]\n", &or);
	or = printf(   "ORIG>\t[% #+-020p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#+-020p]\n", &or);
	mr = ft_printf("MINE>\t[% '#+-020p]\n", &or);
	or = printf(   "ORIG>\t[% '#+-020p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%#+020p]\n", &or);
	mr = ft_printf("MINE>\t[%#+020p]\n", &or);
	or = printf(   "ORIG>\t[%#+020p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#+020p]\n", &or);
	mr = ft_printf("MINE>\t[% '#+020p]\n", &or);
	or = printf(   "ORIG>\t[% '#+020p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#+20p]\n", &or);
	mr = ft_printf("MINE>\t[% '#+20p]\n", &or);
	or = printf(   "ORIG>\t[% '#+20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#20p]\n", &or);
	mr = ft_printf("MINE>\t[% '#20p]\n", &or);
	or = printf(   "ORIG>\t[% '#20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#p]\n", &or);
	mr = ft_printf("MINE>\t[% '#p]\n", &or);
	or = printf(   "ORIG>\t[% '#p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#p]\n", NULL);
	mr = ft_printf("MINE>\t[% '#p]\n", NULL);
	or = printf(   "ORIG>\t[% '#p]\n", NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#p]\n", 140734573365944);
	mr = ft_printf("MINE>\t[% '#p]\n", 140734573365944);
	or = printf(   "ORIG>\t[% '#p]\n", 140734573365944);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#p]\n", (void *)ft_printf);
	mr = ft_printf("MINE>\t[% '#p]\n", (void *)ft_printf);
	or = printf(   "ORIG>\t[% '#p]\n", (void *)ft_printf);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#p %%p]\n", (void *)ft_printf, NULL);
	mr = ft_printf("MINE>\t[% '#p %p]\n", (void *)ft_printf, NULL);
	or = printf(   "ORIG>\t[% '#p %p]\n", (void *)ft_printf, NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#10p %%-5p]\n", test, NULL);
	mr = ft_printf("MINE>\t[% '#10p %-5p]\n", test, NULL);
	or = printf(   "ORIG>\t[% '#10p %-5p]\n", test, NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	free(test);
	test = 4587;
	ft_printf("TEST>\t[%% '#10p %%-5p]\n", test, NULL);
	mr = ft_printf("MINE>\t[% '#10p %-5p]\n", test, NULL);
	or = printf(   "ORIG>\t[% '#10p %-5p]\n", test, NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#p %%p]\n", NULL);
	mr = ft_printf("MINE>\t[% '#p %p]\n", NULL);
	or = printf(   "ORIG>\t[% '#p %p]\n", NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% '#p %%p]\n", 0234234, 'b');
	mr = ft_printf("MINE>\t[% '#p %p]\n", 0234234, 'b');
	or = printf(   "ORIG>\t[% '#p %p]\n", 0234234, 'b');
	num++;
	all += print_result(mr, or, __LINE__);

	printf("\033[1;37m==== %%p + modifiers ===\033[0m\n");

	ft_printf("TEST>\t[%%hhp]\n", (void *)ft_printf, NULL);
	mr = ft_printf("MINE>\t[%hhp]\n", (void *)ft_printf, NULL);
	or = printf(   "ORIG>\t[%hhp]\n", (void *)ft_printf, NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("MINE>\t[%%hp]\n", (void *)ft_printf, NULL);
	mr = ft_printf("MINE>\t[%hp]\n", (void *)ft_printf, NULL);
	or = printf(   "ORIG>\t[%hp]\n", (void *)ft_printf, NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%lp]\n", (void *)ft_printf, NULL);
	mr = ft_printf("MINE>\t[%lp]\n", (void *)ft_printf, NULL);
	or = printf(   "ORIG>\t[%lp]\n", (void *)ft_printf, NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%llp]\n", (void *)ft_printf, NULL);
	mr = ft_printf("MINE>\t[%llp]\n", (void *)ft_printf, NULL);
	or = printf(   "ORIG>\t[%llp]\n", (void *)ft_printf, NULL);
	num++;
	all += print_result(mr, or, __LINE__);

	// printf("\033[35m");
	ft_printf("TEST>\t[%%jp]\n", (void *)ft_printf, NULL);
	mr = ft_printf("MINE>\t[%jp]\n", (void *)ft_printf, NULL);
	or = printf(   "ORIG>\t[%jp]\n", (void *)ft_printf, NULL);
	num++;
	all += print_result(mr, or, __LINE__);
	// printf("\033[33mTest not executed\n\n\033[0m");

	// printf("\033[35m");
	ft_printf("TEST>\t[%%tp]\n", (void *)ft_printf, NULL);
	mr = ft_printf("MINE>\t[%tp]\n", (void *)ft_printf, NULL);
	or = printf(   "ORIG>\t[%tp]\n", (void *)ft_printf, NULL);
	num++;
	all += print_result(mr, or, __LINE__);
	// printf("\033[33mTest not executed\n\n\033[0m");

	// printf("\033[35m");
	ft_printf("TEST>\t[%%zp]\n", (void *)ft_printf, NULL);
	mr = ft_printf("MINE>\t[%zp]\n", (void *)ft_printf, NULL);
	or = printf(   "ORIG>\t[%zp]\n", (void *)ft_printf, NULL);
	num++;
	all += print_result(mr, or, __LINE__);
	// printf("\033[33mTest not executed\n\n\033[0m");

	ft_printf("TEST>\t[%%lld]\n", 9223372036854775807);
	mr = ft_printf("MINE>\t[%lld]\n", 9223372036854775807);
	or = printf(   "ORIG>\t[%lld]\n", 9223372036854775807);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%lli]\n", 9223372036854775807);
	mr = ft_printf("MINE>\t[%lli]\n", 9223372036854775807);
	or = printf(   "ORIG>\t[%lli]\n", 9223372036854775807);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%llu]\n", 18446744073709551615);
	mr = ft_printf("MINE>\t[%llu]\n", 18446744073709551615);
	or = printf(   "ORIG>\t[%llu]\n", 18446744073709551615);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%50.20p]\n", &or);
	mr = ft_printf("MINE>\t[%50.20p]\n", &or);
	or = printf(   "ORIG>\t[%50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%-50.20p]\n", &or);
	mr = ft_printf("MINE>\t[%-50.20p]\n", &or);
	or = printf(   "ORIG>\t[%-50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%+-50.20p]\n", &or);
	mr = ft_printf("MINE>\t[%+-50.20p]\n", &or);
	or = printf(   "ORIG>\t[%+-50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%#50.20p]\n", &or);
	mr = ft_printf("MINE>\t[%#50.20p]\n", &or);
	or = printf(   "ORIG>\t[%#50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%#-50.20p]\n", &or);
	mr = ft_printf("MINE>\t[%#-50.20p]\n", &or);
	or = printf(   "ORIG>\t[%#-50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%#+-50.20p]\n", &or);
	mr = ft_printf("MINE>\t[%#+-50.20p]\n", &or);
	or = printf(   "ORIG>\t[%#+-50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% 50.20p]\n", &or);
	mr = ft_printf("MINE>\t[% 50.20p]\n", &or);
	or = printf(   "ORIG>\t[% 50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% -50.20p]\n", &or);
	mr = ft_printf("MINE>\t[% -50.20p]\n", &or);
	or = printf(   "ORIG>\t[% -50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% +-50.20p]\n", &or);
	mr = ft_printf("MINE>\t[% +-50.20p]\n", &or);
	or = printf(   "ORIG>\t[% +-50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% #50.20p]\n", &or);
	mr = ft_printf("MINE>\t[% #50.20p]\n", &or);
	or = printf(   "ORIG>\t[% #50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% #-50.20p]\n", &or);
	mr = ft_printf("MINE>\t[% #-50.20p]\n", &or);
	or = printf(   "ORIG>\t[% #-50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%% #+-50.20p]\n", &or);
	mr = ft_printf("MINE>\t[% #+-50.20p]\n", &or);
	or = printf(   "ORIG>\t[% #+-50.20p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("\033[1;37m==== %%p + double attr ===\033[0m\n");

	ft_printf("TEST>\t[%%-20+p]\n", &or);
	mr = ft_printf("MINE>\t[%-20+p]\n", &or);
	or = printf(   "ORIG>\t[%-20+p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%-20+lp]\n", &or);
	mr = ft_printf("MINE>\t[%-20+lp]\n", &or);
	or = printf(   "ORIG>\t[%-20+lp]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	ft_printf("TEST>\t[%%-20+-p]\n", &or);
	mr = ft_printf("MINE>\t[%-20+-p]\n", &or);
	or = printf(   "ORIG>\t[%-20+-p]\n", &or);
	num++;
	all += print_result(mr, or, __LINE__);

	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end %%p ====\033[0m\n\n");
	return (all);
}
