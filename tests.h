/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tests.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 11:28:52 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/01 18:12:02 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TESTS_H
# define TESTS_H

# include <stdio.h>
# include <stdlib.h>
# include <locale.h>
# include "/Users/edjubert/Documents/rendu/ft_printf/includes/ft_printf.h"

int	test_digits(void);
int	test_unsigned(void);
int	test_strings(void);
int	test_chars(void);
int	test_octal(void);
int	test_hexadecimal(void);
int	test_hexadecimal_hard(void);
int	test_memory(void);
int	test_width_parameters(void);
int	test_crashtest(void);
int	test_modifiers(void);
int	test_precision(void);
int	test_float(void);
int	test_float_long(void);
int	test_filechecker(void);
int	print_result(int mr, int ori, int line);

#endif
