/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.hexadecimal.hard.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 14:50:07 by edjubert          #+#    #+#             */
/*   Updated: 2019/02/28 16:05:45 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

int	test_hexadecimal_hard(void)
{
	int
	mr, or, all, num;

	mr = 0;
	or = 0;
	all = 0;
	num = 0;

	printf("\n\033[1;37m=[HARD]= %%x and %%X ========\033[0m\n\n");

	printf("TEST>\t[%%#+9x] ->  177215\n");
	mr = ft_printf("MINE>\t[%#+9x]\n", 177215);
	or = printf("ORIG>\t[%#+9x]\n", 177215);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#-15X] ->  167715\n");
	mr = ft_printf("MINE>\t[%#-15X]\n", 167715);
	or = printf(   "ORIG>\t[%#-15X]\n", 167715);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%# +9x] ->  17215\n");
	mr = ft_printf("MINE>\t[%# +9x]\n", 17215);
	or = printf(   "ORIG>\t[%# +9x]\n", 17215);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#'-15X] ->  167715\n");
	mr = ft_printf("MINE>\t[%#'-15X]\n", 167715);
	or = printf(   "ORIG>\t[%#'-15X]\n", 167715);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%020x] ->  1254789652\n");
	mr = ft_printf("MINE>\t[%020x]\n", 1254789652);
	or = printf(   "ORIG>\t[%020x]\n", 1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#020x] ->  1254789652\n");
	mr = ft_printf("MINE>\t[%#020x]\n", 1254789652);
	or = printf(   "ORIG>\t[%#020x]\n", 1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#20.10x] ->  1254789652\n");
	mr = ft_printf("MINE>\t[%#20.10x]\n", 1254789652);
	or = printf(   "ORIG>\t[%#20.10x]\n", 1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#10.20x] ->  1254789652\n");
	mr = ft_printf("MINE>\t[%#10.20x]\n", 1254789652);
	or = printf(   "ORIG>\t[%#10.20x]\n", 1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#10.20x] ->  1254789652\n");
	mr = ft_printf("MINE>\t[%#10.20x]\n", 1254789652);
	or = printf(   "ORIG>\t[%#10.20x]\n", 1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#10.20x] ->  1254789652\n");
	mr = ft_printf("MINE>\t[%#10.20x]\n", 1254789652);
	or = printf(   "ORIG>\t[%#10.20x]\n", 1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#.1x] ->  1254789652\n");
	mr = ft_printf("MINE>\t[%#.1x]\n", 1254789652);
	or = printf(   "ORIG>\t[%#.1x]\n", 1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#+020x] ->  1254789652\n");
	mr = ft_printf("MINE>\t[%#+020x]\n", 1254789652);
	or = printf(   "ORIG>\t[%#+020x]\n", 1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#+20x] ->  1254789652\n");
	mr = ft_printf("MINE>\t[%#+20x]\n", 1254789652);
	or = printf(   "ORIG>\t[%#+20x]\n", 1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#+20x] ->  0xffffff\n");
	mr = ft_printf("MINE>\t[%#+20x]\n", 0xffffff);
	or = printf(   "ORIG>\t[%#+20x]\n", 0xffffff);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#+20x] ->  0xffffff\n");
	mr = ft_printf("MINE>\t[%#+20x]\n", 0xffffff);
	or = printf(   "ORIG>\t[%#+20x]\n", 0xffffff);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#10.20x] ->  -1254\n");
	mr = ft_printf("MINE>\t[%#10.20x]\n", -1254);
	or = printf(   "ORIG>\t[%#10.20x]\n", -1254);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#+10.20x] ->  -1254789652\n");
	mr = ft_printf("MINE>\t[%#+10.20x]\n", -1254789652);
	or = printf(   "ORIG>\t[%#+10.20x]\n", -1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%#+-10.20x] ->  -1254789652\n");
	mr = ft_printf("MINE>\t[%#+-10.20x]\n", -1254789652);
	or = printf(   "ORIG>\t[%#+-10.20x]\n", -1254789652);
	num++;
	all += print_result(mr, or, __LINE__);

	printf("TEST>\t[%%020x] ->  1254789652\n");
	mr = ft_printf("MINE>\t[%020x]\n", 1254789652);
	or = printf(   "ORIG>\t[%020x]\n", 1254789652);
	num++;
	all += print_result(mr, or, __LINE__);
	if (num == all)
		printf("\n\033[1;32m%d/%d tests passed\033[0m\n", all, num);
	else
		printf("\n\033[1;31m%d/%d tests passed\033[0m\n", all, num);
	printf("\033[1;37m======== end %%x and %%X ====\033[0m\n\n");
	return (all);
}
